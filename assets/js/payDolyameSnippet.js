/**
 * @file
 * Author: Synapse-studio.
 */

(function ($) {
  "use strict";

  const DRAGTHRESHOLD = window.innerWidth > 1024 ? 20 : 0;

  const myDolyameBlock = (partPrice, selectorBlock) => {
    const modalDetailsOfPayment = document.querySelector(selectorBlock);

    if (!modalDetailsOfPayment) {
      return;
    }

    const monthNames = [
      "янв",
      "февр",
      "мар",
      "апр",
      "май",
      "июн",
      "июл",
      "авг",
      "сент",
      "окт",
      "ноя",
      "дек",
    ];

    const curDate = new Date(); //текущая дата - Fri Apr 26 2024 09:07:01 GMT+0300 (Москва, стандартное время)
    const curTime = curDate.getTime(); // 1714112659352 - кол-во миллисекунд, прошедших с 1 января 1970 г

    let newLi, newDivPrice;
    let startTime, nextTime, nextDate, nextDay, nextMonth, nowDate, newDivDate;

    startTime = curTime;
    for (let i = 0; i < 4; i++) {
      newLi = document.createElement("li");
      newLi.classList.add("details-of-payment_item");

      newDivDate = document.createElement("div");
      newDivDate.classList.add("details-of-payment_date");
      if (i == 0) {
        nowDate = "Сегодня";
      } else {
        //2нед = 2 * 7дн = 2*7 * 24ч = 2*7*24 * 60мин = 2*7*24*60 * 60сек = 2*7*24*60*60 * 1000миллисек = 1209600000 миллисек
        nextTime = startTime + 1209600000; // 1715322259352 - кол-во миллисекунд (+ 2 недели)
        nextDate = new Date(nextTime); //Fri May 10 2024 09:26:42 GMT+0300 (Москва, стандартное время)

        nextDay = nextDate.getDate();
        nextMonth = nextDate.getMonth();
        nowDate = `${nextDay} ${monthNames[nextMonth]}`;
        startTime = nextTime;
      }
      newDivDate.textContent = nowDate;
      newLi.append(newDivDate);

      newDivPrice = document.createElement("div");
      newDivPrice.classList.add("details-of-payment_price");
      newDivPrice.textContent = partPrice;
      newLi.append(newDivPrice);

      modalDetailsOfPayment.append(newLi);
    }
  };

  const myDolyameButton = (widgetWrapper, partPrice) => {
    let $pathSynpay = drupalSettings["pathSynpay"];

    if (!widgetWrapper || !$pathSynpay) {
      return;
    }

    // console.log($pathSynpay);

    widgetWrapper.innerHTML = `
      <button class="digi-dolyame-button digi-dolyame-button--3" data-bs-toggle="modal" data-bs-target="#block-modal-mydolyame">
        <img src="/${$pathSynpay}/assets/image/dolyame.svg" alt="dolyame" class="digi-dolyame-button__label">
        <div class="digi-dolyame-button__content">
          <p class="digi-dolyame-button__text">4 платежа по
            <span class="digi-dolyame__details-of-payment_price">${partPrice}</span>
          </p>
        </div>
        <svg width="6" height="11" viewbox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg" class="digi-dolyame-button__icon digi-dolyame-button__icon-arrow">
          <path d="M4.18934 5.25L0.219669 9.21967C-0.0732238 9.51256 -0.0732238 9.98744 0.219669 10.2803C0.512563 10.5732 0.987437 10.5732 1.28033 10.2803L5.78033 5.78033C6.07322 5.48744 6.07322 5.01256 5.78033 4.71967L1.28033 0.219669C0.987438 -0.0732245 0.512564 -0.0732245 0.21967 0.219669C-0.073223 0.512562 -0.073223 0.987436 0.21967 1.28033L4.18934 5.25Z" fill="#9299A2"></path>
        </svg>
      </button>
    `;
  };

  const myDolyameProductFull = () => {
    const widgetWrapper = document.querySelector(
      ".product-full .dolyame_snippet"
    );

    if (!widgetWrapper) {
      return;
    }

    const price = +widgetWrapper.dataset.price;

    const currency = "₽";

    if (!price) {
      return;
    }

    let partPrice = price / 4 + " " + currency;

    myDolyameButton(widgetWrapper, partPrice);

    myDolyameBlock(
      partPrice,
      "#block-modal-mydolyame  .snippetDolyame__details-of-payment"
    );
  };

  const myDolyameCheckout = () => {
    const checkoutYaPayWidget = document.querySelector(
      ".checkout.dolyame_snippet"
    );

    if (!checkoutYaPayWidget || !checkoutYaPayWidget.dataset.checkoutprice) {
      return;
    }

    const price = Number(checkoutYaPayWidget.dataset.checkoutprice);

    const currency = "₽";

    let partPrice = price / 4 + " " + currency;

    const blockDolyameCheckout = document.createElement("div");
    blockDolyameCheckout.classList.add("block-checkout-mydolyame");
    blockDolyameCheckout.innerHTML = `
      <div class="block-checkout-mydolyame__description-details">
        Оплатите 25% от стоимости покупки, а оставшиеся 3 части спишутся
        автоматически с шагом в две недели. <br> Без процентов и комиссий, как обычная оплата картой.
      </div>
      <ul class="snippetDolyame__details-of-payment"></ul>
      <p class="block-checkout-mydolyame__details">
        Подробности на
        <noindex>
          <a href="https://dolyame.ru" target="_blank" rel="nofollow">dolyame.ru</a>
        </noindex>
      </p>
    `;

    checkoutYaPayWidget.append(blockDolyameCheckout);
    myDolyameBlock(partPrice, ".snippetDolyame__details-of-payment");
  };

  $(document).ready(function () {
    let inputDolyameCheckout = document.querySelector(
      '.checkout input[value="tinkoff_dolyame"]'
    );

    myDolyameProductFull();

    if (inputDolyameCheckout && inputDolyameCheckout.checked) {
      myDolyameCheckout();
    }
  });

  if (typeof Drupal === "object") {
    Drupal.behaviors.synpayDolyameSnippet = {
      attach: function (context) {
        if (
          context.classList &&
          context.classList.contains("checkout-pane-payment-information") &&
          context.querySelector("input[value='tinkoff_dolyame']:checked")
        ) {
          myDolyameCheckout();
        }
      },
    };
  }
})(this.jQuery);
