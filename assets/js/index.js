/**
 * @file
 * Simple JavaScript hello world file.
 */

(function ($) {

  "use strict";

  $(document).ready(function () {
    payment();
  });

  function payment() {
    var widget = new cp.CloudPayments();
    widget.charge({
      publicId: drupalSettings.synpay.publicId,
      description: drupalSettings.synpay.description,
      amount: parseFloat(drupalSettings.synpay.amount),
      currency: drupalSettings.synpay.currency,
      accountId: drupalSettings.synpay.accountId,
      invoiceId: drupalSettings.synpay.invoiceId,
      skin: drupalSettings.synpay.skin,
    },
      function (options) { // success
        document.location.reload();
      },
    );
  }

})(jQuery);