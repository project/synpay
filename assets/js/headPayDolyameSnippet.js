(function ($) {

  if (
    !drupalSettings['dolyame_snippet'] ||
    !drupalSettings['dolyame_snippet']['siteId']
  ) {
    console.log("не получен drupalSettings['dolyame_snippet']['siteId']");
    return;
  }
  let $siteId = drupalSettings['dolyame_snippet']['siteId'];

  $(document).ready(function () {
    var fileref = document.createElement('script');
    fileref.setAttribute('type', 'text/javascript');
    fileref.setAttribute(
      'src',
      '//aq.dolyame.ru/' + String($siteId) + '/client.js?ts=' + Date.now()
    );
    fileref.defer = true;
    fileref.async = true;
    document.getElementsByTagName('head')[0].appendChild(fileref);
  });
})(this.jQuery);
