(function ($) {
  $(document).ready(function () {
    var fileref = document.createElement('script');
    fileref.setAttribute('type', 'text/javascript');
    fileref.setAttribute('src', 'https://pay.yandex.ru/sdk/v1/pay.js');
    fileref.onload = function () {
      onYaPayLoad();
    };
    fileref.async = true;
    document.getElementsByTagName('head')[0].appendChild(fileref);
  });
})(this.jQuery);
