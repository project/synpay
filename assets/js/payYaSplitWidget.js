/**
 * @file
 * Simple JavaScript hello world file.
 */

const yaPayWidget = ($totalAmount, $widgetType, $selector) => {
  if (!$totalAmount || !$widgetType || !$selector) {
    return;
  }

  let $merchantId = drupalSettings["yandex_split"]["merchantId"];
  if (!$merchantId) {
    console.log("не получен drupalSettings['yandex_split']['merchantId']");
    return;
  }

  // Данные платежа
  const paymentData = {
    version: 4,
    currencyCode: YaPay.CurrencyCode.Rub,
    merchantId: $merchantId,
    totalAmount: $totalAmount,
    availablePaymentMethods: ["SPLIT"],
  };

  async function onPayButtonClick() {}

  function onFormOpenError(reason) {
    console.error(`Payment error — ${reason}`);
  }

  //сам виджет
  YaPay.createSession(paymentData, {
    onPayButtonClick: onPayButtonClick,
    onFormOpenError: onFormOpenError,
  })
    .then(function (paymentSession) {
      paymentSession.mountWidget($selector, {
        widgetType: $widgetType,
      });
    })
    .catch(function (err) {
      console.log(err);
      // Не получилось создать платежную сессию.
    });
};

const yaPayWidgetProductFull = () => {
  const widgetWrapper = document.querySelector(
    ".product-full .yandex_split_widget"
  );

  const YaPay = window.YaPay;

  if (!widgetWrapper) {
    return;
  }

  widgetWrapper.style.width = "100%";

  $totalAmount = widgetWrapper.dataset.price;
  $widgetType = YaPay.WidgetType.Simple;
  $selector = ".product-full .yandex_split_widget";

  yaPayWidget($totalAmount, $widgetType, $selector);
};

const yaPayWidgetCheckout = () => {
  const checkoutYaPayWidget = document.querySelector(
    ".checkout.yandex_split_widget"
  );

  const YaPay = window.YaPay;

  if (!checkoutYaPayWidget || !checkoutYaPayWidget.dataset.checkoutprice) {
    return;
  }

  checkoutYaPayWidget.style.width = "500px";
  checkoutYaPayWidget.style.height = "220px";

  $totalAmount = Number(checkoutYaPayWidget.dataset.checkoutprice);
  $widgetType = YaPay.WidgetType.Info;
  $selector = ".checkout.yandex_split_widget";

  yaPayWidget($totalAmount, $widgetType, $selector);
  // Далее при каждом переключении метода оплаты форма "Способ оплаты" перерисовывается
  // Поэтому ф-цию yaPayWidgetCheckout нужно каждый раз вызывать снова
  // См. внизу Drupal.behaviors.adaptive
};

// ф-ция вызывается однократно из head
function onYaPayLoad() {
  let inputYandexSplit = document.querySelector(
    '.checkout input[value="yandex_split"]'
  );

  yaPayWidgetProductFull();

  if (inputYandexSplit && inputYandexSplit.checked) {
    yaPayWidgetCheckout();
  }
}

(function ($) {
  "use strict";

  $(document).ready(function () {});

  if (typeof Drupal === "object") {
    Drupal.behaviors.synpayYaSplit = {
      attach: function (context) {
        if (
          context.classList &&
          context.classList.contains("checkout-pane-payment-information") &&
          context.querySelector("input[value='yandex_split']:checked")
        ) {
          yaPayWidgetCheckout();
        }
      },
    };
  }
})(jQuery);
