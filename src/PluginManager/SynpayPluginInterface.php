<?php

namespace Drupal\synpay\PluginManager;

/**
 * Defines the common interface for all Docker classes.
 *
 * @see \Drupal\synpay\PluginManager\SynpayGatewayManager
 * @see \Drupal\synpay\PluginManager\SynpayAnnotation
 * @see plugin_api
 */
interface SynpayPluginInterface {

}
