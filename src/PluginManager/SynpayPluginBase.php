<?php

namespace Drupal\synpay\PluginManager;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides an Docker plugin manager.
 *
 * @see Drupal\synpay\PluginManager\SynpayAnnotation
 * @see Drupal\synpay\PluginManager\SynpayPluginInterface
 * @see plugin_api
 */
class SynpayPluginBase extends PluginBase implements SynpayPluginInterface {

  /**
   * Формирование набора параметров для шлюза.
   */
  public function makeSettings($key) {
    $data = [];
    $config = \Drupal::config('synpay.settings');
    foreach ($this->fieldset as $fieldSuffix) {
      $field = "{$key}_{$fieldSuffix}";
      $data[$fieldSuffix] = $this->active ? $config->get($field, '') : '';
    }
    return (object) $data;
  }

}
