<?php

namespace Drupal\synpay\PluginManager;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an synpay template object.
 *
 * Plugin Namespace: Plugin\ZappDocker.
 *
 * For a working example, see \Drupal\system\Plugin\Docker\MySql
 *
 * @see \Drupal\synpay\PluginManager\SynpayGatewayManager
 * @see \Drupal\synpay\PluginManager\SynpayPluginInterface
 * @see plugin_api
 * @see hook_archiver_info_alter()
 *
 * @Annotation
 */
class SynpayAnnotation extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
