<?php

namespace Drupal\synpay\Hook;

/**
 * Hook preprocess page class.
 */
class PreprocessCommerceProduct {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    $config_synpay = \Drupal::config('synpay.settings');

    $variables['cart']['#data']['settings']['widget']['dolyame_snippet'] = ($config_synpay->get('tinkoff_dolyame_active') == '1' && $config_synpay->get('tinkoff_dolyame_snippet') == 'custom');
    $variables['cart']['#data']['settings']['widget']['yandex_split_widget'] = ($config_synpay->get('yandex_split_active') == '1' && $config_synpay->get('yandex_split_widget') == '1' && (!empty($config_synpay->get('yandex_split_live_login')) || !empty($config_synpay->get('yandex_split_test_login'))));
  }

}
