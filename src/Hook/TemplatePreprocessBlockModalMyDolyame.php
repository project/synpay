<?php

namespace Drupal\synpay\Hook;

/**
 * @file
 * Contains \Drupal\synpay\Hook\TemplatePreprocessBlockModalMyDolyame.
 */

/**
 * Theme.
 */
class TemplatePreprocessBlockModalMyDolyame {

  /**
   * Hook.
   */
  public static function hook(&$variables) {
    template_preprocess_block($variables);
    $variables['module_path'] = \Drupal::service('module_handler')->getModule('synpay')->getPath();
  }

}
