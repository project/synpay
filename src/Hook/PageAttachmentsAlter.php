<?php

namespace Drupal\synpay\Hook;

/**
 * @file
 * Contains \Drupal\synpay\Hook.
 */

/**
 * Class PageAttachmentsAlter.
 */
class PageAttachmentsAlter {

  /**
   * Hook.
   */
  public static function hook(&$attachments) {
    $route_name = \Drupal::service('current_route_match')->getRouteName();
    if (in_array($route_name, ['commerce_checkout.form', 'entity.commerce_product.canonical'])) {
      $config = \Drupal::config('synpay.settings');

      if ($config->get('yandex_split_active') == '1' && $config->get('yandex_split_widget') == '1' && (!empty($config->get('yandex_split_live_login')) || !empty($config->get('yandex_split_test_login')))) {
        if (!empty($config->get('yandex_split_live_login'))) {
          $merchantId = $config->get('yandex_split_live_login');
        }
        else {
          $merchantId = $config->get('yandex_split_test_login');
        }
        $attachments['#attached']['drupalSettings']['yandex_split']['merchantId'] = $merchantId;
        $attachments['#attached']['library'][] = 'synpay/synpay-yandex-split';
      }
      if ($config->get('tinkoff_dolyame_active') == '1') {
        switch ($config->get('tinkoff_dolyame_snippet')) {
          case 'original':
            // dsm('original');.
            if (!empty($config->get('tinkoff_dolyame_siteID'))) {
              $siteID = $config->get('tinkoff_dolyame_siteID');
              $attachments['#attached']['drupalSettings']['dolyame_snippet']['siteId'] = $siteID;
              $attachments['#attached']['library'][] = 'synpay/synpay-dolyame-snippet';
            }
            break;

          case 'custom':
            $pathSynpay = \Drupal::service('module_handler')->getModule('synpay')->getPath();
            $attachments['#attached']['drupalSettings']['pathSynpay'] = $pathSynpay;
            $attachments['#attached']['library'][] = 'synpay/synpay-dolyame-snippet-synapse';
            break;

          default:
            break;
        }
      }
    }
  }

}
