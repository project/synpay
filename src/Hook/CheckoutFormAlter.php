<?php

namespace Drupal\synpay\Hook;

use Drupal\Core\Form\FormStateInterface;

/**
 * CheckoutFormAlter.
 */
class CheckoutFormAlter {

  /**
   * Hook From Alter.
   */
  public static function hook(&$form, FormStateInterface $form_state, $form_id) {
    // Step 1 Order Information.
    if ($form['#step_id'] == 'order_information') {
      $config_synpay = \Drupal::config('synpay.settings');
      // Это текущий заказ.
      $current_order = \Drupal::routeMatch()->getParameters()->get('commerce_order');
      // Это корзина.
      $cart_provider = \Drupal::service('commerce_cart.cart_provider');
      $carts = $cart_provider->getCarts();
      $current_cart = array_shift($carts);
      if ($current_order->id() == $current_cart->id()) {
        $total_price = intval($current_cart->getTotalPrice()->getNumber());
        if ($config_synpay->get('yandex_split_active') == '1' && $config_synpay->get('yandex_split_widget') == '1') {
          if (!empty($form['payment_information']['payment_method']['yandex_split'])) {
            $form['payment_information']['payment_method']['yandex_split']['#suffix'] = '<div class="checkout yandex_split_widget" data-checkoutprice="' . $total_price . '"></div>';
          }
        }
        if ($config_synpay->get('tinkoff_dolyame_active') == '1' && $config_synpay->get('tinkoff_dolyame_snippet') == 'custom') {
          if (!empty($form['payment_information']['payment_method']['tinkoff_dolyame'])) {
            $form['payment_information']['payment_method']['tinkoff_dolyame']['#suffix'] = '<div class="checkout dolyame_snippet" data-checkoutprice="' . $total_price . '"></div>';
          }
        }

      }
    }
    // Step 2 Review.
    if ($form['#step_id'] == 'review') {
      $order = \Drupal::routeMatch()->getParameters()->get('commerce_order');
      if ($order->hasField('payment_gateway')) {
        $pluginConfiguration = [];
        if ($payment_gateway = $order->payment_gateway->entity) {
          $pluginConfiguration = $payment_gateway->getPluginConfiguration();
        }
        if (!empty($pluginConfiguration) && $pluginConfiguration['mode'] == 'test') {
          $manager = \Drupal::service('plugin.manager.synpay');
          $text = "<b>Оплаты работают в тестовом режиме!</b><br>";
          if (isset($pluginConfiguration['gateway'])) {
            $synpay = $manager->createInstance($pluginConfiguration['gateway']);
            $text .= $synpay->testCards();
          }
          $text = "<div class='field__item'>$text</div><br>";
          $form['review']['payment_information']['summary']['payment_gateway']['#prefix'] = $text;
        }
      }
    }
    // Step 3 Payment.
    if ($form['#step_id'] == 'payment') {
    }
    // Step 4 Complete.
    if ($form['#step_id'] == 'complete') {
    }
  }

}
