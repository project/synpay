<?php

namespace Drupal\synpay\Hook;

/**
 * @file
 * Contains \Drupal\synpay\Hook\Theme.
 */

/**
 * Theme.
 */
class Theme {

  /**
   * Hook.
   */
  public static function hook() {
    return [
      'synpay' => [
        'render element' => 'children',
      ],
      'block__modal_mydolyame' => [
        'template' => 'block--modal-mydolyame',
        'variables' => ['data' => []],
      ],
    ];
  }

}
