<?php

namespace Drupal\synpay\Service;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GatewayService.
 */
class GatewayService {

  /**
   * Constructs a new GatewayService object.
   */
  public function __construct() {
    $manager = \Drupal::service('plugin.manager.synpay');
    $this->manager = $manager;
    $this->definitions = $manager->getDefinitions();
  }

  /**
   * Commerce Return.
   */
  public function init(string $mode = 'test', array $config = []) {
    $config['mode'] = $mode;
    $this->gateway = $this->manager->createInstance($config['gateway'], $config);
    return $this;
  }

  /**
   * Commerce Place Order.
   */
  public function registerOrder(string $order_id, PaymentInterface $payment, array $params = []) {
    return $this->gateway->registerOrder($order_id, $payment, $params);
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    return $this->gateway->onReturn($order, $request);
  }

  /**
   * Commerce Notify.
   */
  public function onNotify(Request $request) {
    return $this->gateway->onNotify($request);
  }

  /**
   * Цена в копейках или с копейками.
   */
  public function getPrecision() {
    return $this->gateway::PRECISION;
  }

  /**
   * SettingsForm.
   */
  public function getSettingsForm() {
    return $this->gateway->settingForm();
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('synpay')->info($message);
  }

}
