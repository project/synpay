<?php

namespace Drupal\synpay\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Order registration and redirection to payment URL.
 */
class OffsitePaymentForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    // If payment is not have ID, we force save it, since it used in $order_id
    // which is required for payment.
    if ($payment->isNew()) {
      $payment->save();
    }
    $order = $payment->getOrder();
    $mode = $this->plugin->getMode();
    $config = $this->getPluginConfig($payment);
    $currency = $this->getPluginCurrencyCode($payment);
    $synpay = \Drupal::service('synpay.gateway')->init($mode, $config);
    $precision = $synpay->getPrecision() ?? TRUE;
    // Use Payment ID instead of actual order id for good API-ID increment.
    $order_id = "{$config['order_id_prefix']}{$order->id()}/{$payment->id()}{$config['order_id_suffix']}";
    // $email = $order->getBillingProfile()->get('field_customer_email')->getString();
    $email = $order->getEmail();
    $params = [
      'currency' => $currency,
      'returnUrl' => $form['#return_url'],
      'failUrl' => $form['#cancel_url'],
      'orderBundle' => [
        'customerDetails' => [
          'email' => $email,
        ],
        'cartItems' => [
          'items' => [],
        ],
      ],
    ];
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $quantity = $item->getQuantity();
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = $precision ? $price : bcmul($price, 100, 0);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = $precision ? $total : bcmul($total, 100, 0);
      $variation = $item->getPurchasedEntity();
      $product = $variation->getProduct();
      $unit = 'шт';
      if ($product->hasField('field_unit')) {
        $unit = $product->field_unit->entity->getName();
      }

      switch ($synpay->gateway->getPluginId()) {
        case 'robokassa':
          $items[] = [
            'name' => $item->getTitle(),
            'quantity' => $quantity,
            'sum' => $total,
            'tax' => 'none',
            // 'nomenclature_code' => $variation->id(),
          ];
          break;

        default:
          $items[] = [
            'positionId' => $c++,
            'name' => $item->getTitle(),
            'itemCode' => $item->id(),
            'quantity' => [
              'value' => str_replace(',', '.', $quantity),
              'measure' => $unit,
            ],
            'itemPrice' => $price,
            'itemAmount' => $total,
          ];
          break;
      }
    }

    $adjustmentItems = $this->getAdjustmentItems($payment, $precision);
    switch ($synpay->gateway->getPluginId()) {
      case 'robokassa':
        foreach ($adjustmentItems as $adjustmentItem) {
          if ($adjustmentItem['itemAmount'] != 0) {
            $items[] = [
              'name' => $adjustmentItem['name'],
              'quantity' => $adjustmentItem['quantity']['value'],
              'sum' => $adjustmentItem['itemAmount'],
              'tax' => 'none',
            ];
          }
        }
        break;

      default:
        foreach ($adjustmentItems as $adjustmentItem) {
          if ($adjustmentItem['itemAmount'] != 0) {
            $adjustmentItem['positionId'] = $c++;
            $items[] = $adjustmentItem;
          }
        }
        break;
    }

    $params['orderBundle']['cartItems']['items'] = $items;
    switch ($synpay->gateway->getPluginId()) {
      case 'sberinstallment':
        $params['orderBundle']['installments'] = [
          'productType' => 'INSTALLMENT',
          'productID' => 10,
          'rightTerms' => [3, 6, 9],
        ];
        break;

      case 'sbercredit':
        $params['orderBundle']['installments'] = [
          'productType' => 'CREDIT',
          'productID' => 10,
        ];
        break;
    }

    $pay = $synpay->registerOrder($order_id, $payment, $params);
    if ($pay) {
      $payment->setAuthorizedTime(time());
      $payment->setRemoteId($pay['orderId']);
      $payment->setState('authorization');
      $payment->save();
      switch ($synpay->gateway->getPluginId()) {
        case 'robokassa':
          return $this->buildRedirectForm($form, $form_state, $pay['url'], $pay['data'], self::REDIRECT_POST);

        default:
          return $this->buildRedirectForm($form, $form_state, $pay['formUrl'], [], self::REDIRECT_GET);
      }
    }
    else {
      \Drupal::logger('synpay')->error("Order: {$order->id()} / Payment {$payment->id()} fails.");
      // Mark payment is failed.
      $payment->setState('authorization_voided');
      $payment->save();
    }
    return $form;
  }

  /**
   * Получить корректировки цены.
   */
  private function getAdjustmentItems(PaymentInterface $payment, $precision) : array {
    $order = $payment->getOrder();
    $adjustments = array_map(function ($adjustment) use ($precision) {
      $amount = $adjustment->getAmount()->getNumber();
      $amount = $precision ? $amount : bcmul($amount, 100, 0);
      if (empty($amount)) {
        return NULL;
      }
      $data = [
        'name' => $adjustment->getLabel(),
        'itemCode' => implode('-', [
          $adjustment->getType(),
          $adjustment->getSourceId(),
        ]),
        'quantity' => [
          'value' => 1,
          'measure' => 'шт',
        ],
        'itemPrice' => $amount,
        'itemAmount' => $amount,
      ];
      switch ($adjustment->getType()) {
        case 'shipping':
          $data['itemAttributes'] = [
            'attributes' => [
              ['name' => 'paymentObject', 'value' => 4],
            ],
          ];
          break;
      }
      return $data;
    }, $order->getAdjustments());
    return array_filter($adjustments);
  }

  /**
   * Plugin currency.
   */
  private function getPluginCurrencyCode(PaymentInterface $payment) : string {
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_currency');
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
    $currency = $storage->load($currency_code);
    return $currency->getNumericCode();
  }

  /**
   * Plugin config.
   */
  private function getPluginConfig(PaymentInterface $payment) : array {
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configs = $payment_gateway_plugin->getConfiguration();
    // Get username and password for payment method.
    // $gw = $configs['gw'];.
    return [
      'gateway' => $configs['gateway'],
      'order_id_prefix' => "",
      'order_id_suffix' => "",
    ];
  }

}
