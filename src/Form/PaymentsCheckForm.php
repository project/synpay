<?php

namespace Drupal\synpay\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Payments check form.
 */
class PaymentsCheckForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synpay_pay_check';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $commerce_order = NULL) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $commerce_order->id()]);
    $form_state->setValue('order_id', $commerce_order->id());
    $form['payments'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => 'Оплата', 'class' => ['text-left']],
        ['data' => 'Состояние', 'class' => ['text-right']],
        ['data' => 'Платёжный шлюз', 'class' => ['text-right']],
        ['data' => 'Авторизованный', 'class' => ['text-right']],
        ['data' => 'Завершено', 'class' => ['text-right']],
        ['data' => 'Внешний ID', 'class' => ['text-right']],
        ['data' => 'Ответ банка', 'class' => ['text-right']],
      ],
    ];
    $currency_formatter = \Drupal::service('commerce_price.currency_formatter');

    foreach ($payments as $payment) {
      $currency_code = $payment->getAmount()->getCurrencyCode();
      $amount = $currency_formatter->format($payment->getAmount()->getNumber(), $currency_code, []);
      $authorizedTime = !empty($payment->getAuthorizedTime()) ? \Drupal::service('date.formatter')->format($payment->getAuthorizedTime(), 'short') : '';
      $completedTime = !empty($payment->getCompletedTime()) ? \Drupal::service('date.formatter')->format($payment->getCompletedTime(), 'short') : '';
      $form['payments']['#rows'][$payment->id()] = [
        $amount,
        $payment->getState()->getLabel(),
        $payment->getPaymentGateway()->label(),
        $authorizedTime,
        $completedTime,
        $payment->getRemoteId(),
        ['data' => '' , 'class' => ["bank-answer{$payment->id()}"]],
      ];
    }
    $form['aсtions'] = [
      '#type' => 'actions',
      'get_status' => [
        '#type' => 'button',
        '#value' => $this->t('Get Status'),
        '#attributes' => ['class' => ['inline', 'btn-success']],
        '#ajax'   => [
          'callback' => '::ajaxCheckOrderStatus',
          'progress' => ['type' => 'throbber', 'message' => NULL],
        ],
      ],
      'update_status' => [],
    ];
    if (\Drupal::currentUser()->id() == 1) {
      $form['aсtions']['update_status'] = [
        '#type' => 'button',
        '#value' => $this->t('Update Status (НЕ НАЖИМАТЬ!!!)'),
        '#attributes' => ['class' => ['inline', 'btn-success']],
        '#ajax'   => [
          'callback' => '::ajaxUpdateOrderStatus',
          'progress' => ['type' => 'throbber', 'message' => NULL],
        ],
      ];
    }
    return $form;
  }

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxCheckOrderStatus(array &$form, $form_state) {
    $response = new AjaxResponse();
    $orderId = $form_state->getValues()['order_id'];
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $orderId]);
    $manager = \Drupal::service('plugin.manager.synpay');
    foreach ($payments as $payment) {
      if (!empty($payment->getRemoteId())) {
        $pluginId = $payment->getPaymentGateway()->getPlugin()->getConfiguration()['gateway'];
        $plugin = $manager->createInstance($pluginId, [
          'order' => $orderId,
        ]);
        $result = $plugin->checkOrderStatus($payment->getRemoteId());
      }
      else {
        $result['status'] = 'Не переходили в банк';
      }
      $response->addCommand(new HtmlCommand(".bank-answer{$payment->id()}", $result['status']));
    }
    return $response;
  }

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxUpdateOrderStatus(array &$form, $form_state) {
    $response = new AjaxResponse();
    $orderId = $form_state->getValues()['order_id'];
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $orderId]);
    $manager = \Drupal::service('plugin.manager.synpay');
    foreach ($payments as $payment) {
      if (!empty($payment->getRemoteId())) {
        $pluginId = $payment->getPaymentGateway()->getPlugin()->getConfiguration()['gateway'];
        $plugin = $manager->createInstance($pluginId, [
          'order' => $orderId,
        ]);
        $result = $plugin->updateOrderStatus($payment->getRemoteId());
      }
      else {
        $result['status'] = 'Не переходили в банк';
      }
      $response->addCommand(new HtmlCommand(".bank-answer{$payment->id()}", $result['status']));
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
