<?php

namespace Drupal\synpay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Payment Gateway Gonfig.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'synpay.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synpay_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('synpay.settings');
    $manager = \Drupal::service('plugin.manager.synpay');
    $gateways = $manager->getDefinitions();
    ksort($gateways);
    foreach ($gateways as $id => $gateway) {
      $synpay = $manager->createInstance($id);
      $label = $gateway['title']->getUntranslatedString();
      $form[$id] = [
        '#type' => 'details',
        '#title' => $label,
        '#open' => $config->get("{$id}_active"),
      ];
      $form[$id]["{$id}_active"] = [
        '#type' => 'checkbox',
        '#title' => "Платежная система $label активна",
        '#default_value' => $config->get("{$id}_active"),
      ];
      foreach ($synpay->settingForm() as $key => $value) {
        $form[$id][$key] = $value;
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('synpay.settings');
    $values = $form_state->getValues();
    $manager = \Drupal::service('plugin.manager.synpay');
    $gateways = $manager->getDefinitions();
    ksort($gateways);
    $data = [];
    foreach ($gateways as $id => $gateway) {
      foreach ($values as $key => $value) {
        if (stripos($key, "{$id}_") !== FALSE) {
          $data[$key] = trim($value);
        }
      }
    }
    $config->setData($data)->save();
  }

}
