<?php

namespace Drupal\synpay\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure zakaz settings for this site.
 */
class BuyNowForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synpay_buy_now';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $form_state->setCached(FALSE);
    $vid = $extra['variation_id'];
    $form_state->setValue('variation', $vid);
    $form['#prefix'] = "<hr>";
    $form['vid'] = [
      '#type' => 'hidden',
      '#value' => $vid,
    ];
    $form['btn'] = [
      '#type' => 'submit',
      '#prefix' => "Buy {$vid}",
      '#attributes' => [
        'class' => [
          'btn',
          'btn-success',
        ],
      ],
      '#value' => $this->t('Submit payment'),
    ];
    $form['btn-ajax'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit ajax'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-info',
        ],
      ],
      '#ajax'   => [
        'callback' => '::submitFormAjax',
        'effect'   => 'fade',
        'progress' => ['type' => 'throbber', 'message' => ""],
      ],
      '#suffix' => '<div id="buy-now"></div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vid = $form_state->getValue('vid');
    if (!\Drupal::request()->request->get('_drupal_ajax')) {
      $id = $this->cart($vid);
      $redirect = "/checkout/$id/payment";
      $form_state->setRedirect('commerce_checkout.form', [
        'commerce_order' => $id,
        // 'step' => 'order_information',
        'step' => 'rewiew',
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $vid = $form_state->getValue('vid');
    $id = $this->cart($vid);
    $redirect = "/checkout/$id/payment";
    $response->addCommand(new HtmlCommand("#buy-now", "+$vid ==> $redirect"));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  private function cart(int $vid) {
    $orderType = 'online';
    $provider = \Drupal::service('commerce_cart.cart_provider');
    // Clear ALL {$orderType} carts.
    $carts = $provider->getCarts();
    if (count($carts)) {
      foreach ($carts as $key => $cart) {
        if ($cart->bundle() == $orderType) {
          $cart->delete();
        }
      }
    }
    $provider->clearCaches();
    $cart = $provider->createCart($orderType);
    $this->addItem($cart, $vid);
    $id = $cart->id();
    return $id;
  }

  /**
   * Добавить товар в корзину.
   */
  private function addItem($cart, $vid, $quantity = 1) {
    $variation = \Drupal::entityTypeManager()
      ->getStorage('commerce_product_variation')
      ->load($vid);
    if (is_object($variation)) {
      $orderItem = \Drupal::entityTypeManager()
        ->getStorage('commerce_order_item')
        ->createFromPurchasableEntity($variation, [
          'quantity' => $quantity,
        ]);
      if (is_object($orderItem)) {
        $cart->addItem($orderItem);
        $cart->recalculateTotalPrice();
        $cart->save();
      }
    }
    return $cart;
  }

}
