<?php

namespace Drupal\synpay\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure zakaz settings for this site.
 */
class PayForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synpay_pay';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    // $plugin = $extra;
    // dsm($extra);
    $form_state->set('paymentPlugin', $extra['plugin']);
    if ($extra['plugin']->mode == 'test') {
      $form['notice'] = [
        '#type' => 'textfield',
        '#title' => $this->t('У Вас тестовый логин. Карта для использования:'),
        '#default_value' => $this->t('Номер: 4111 1111 1111 1111, Срок действия: 12/24, CVC2/CVV2 код: 123, Пароль: 12345678'),
      ];
      $form['notice']['#attributes'] = ['readonly' => 'readonly'];
    }
    $form['order'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order number'),
      '#default_value' => $extra['order'],
    ];
    $form['order']['#attributes'] = ['readonly' => 'readonly'];
    $form['total'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Order total'),
      '#default_value' => $extra['total'],
    ];
    $form['btn'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit payment'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-warning',
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin = $form_state->get('paymentPlugin');
    $total = $form_state->getValue('total');
    $order = $form_state->getValue('order');
    $plugin->pay($order, $total);
  }

}
