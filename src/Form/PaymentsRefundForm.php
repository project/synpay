<?php

namespace Drupal\synpay\Form;

use Drupal\commerce_price\Price;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Payments refund form.
 */
class PaymentsRefundForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synpay_pay_refund';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $commerce_order = NULL) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $commerce_order->id()]);
    $form_state->setValue('order_id', $commerce_order->id());
    $form['payments'] = [
      '#type' => 'table',
      '#weight'  => 0,
      '#header' => [
        ['data' => 'Оплачено', 'class' => ['text-left']],
        ['data' => 'Возвращено', 'class' => ['text-left']],
        ['data' => 'Состояние', 'class' => ['text-right']],
        ['data' => 'Платёжный шлюз', 'class' => ['text-right']],
        ['data' => 'Авторизованный', 'class' => ['text-right']],
        ['data' => 'Завершено', 'class' => ['text-right']],
        ['data' => 'Внешний ID', 'class' => ['text-right']],
      ],
    ];
    $currency_formatter = \Drupal::service('commerce_price.currency_formatter');

    foreach ($payments as $payment) {
      $state = $payment->getState()->getString();
      if (in_array($state, ['completed', 'partially_refunded', 'refunded'])) {
        $currency_code = $payment->getAmount()->getCurrencyCode();
        $amount = $currency_formatter->format($payment->getAmount()->getNumber(), $currency_code, []);
        $refunded = $currency_formatter->format($payment->getRefundedAmount()->getNumber(), $currency_code, []);
        $authorizedTime = !empty($payment->getAuthorizedTime()) ? \Drupal::service('date.formatter')->format($payment->getAuthorizedTime(), 'short') : '';
        $completedTime = !empty($payment->getCompletedTime()) ? \Drupal::service('date.formatter')->format($payment->getCompletedTime(), 'short') : '';
        $form_state->setValue('remote_id', $payment->getRemoteId());
        $form['payments']['#rows'][$payment->id()] = [
          $amount,
          $refunded,
          $payment->getState()->getLabel(),
          $payment->getPaymentGateway()->label(),
          $authorizedTime,
          $completedTime,
          $payment->getRemoteId(),
        ];
      }
    }
    $form['orderItems'] = [
      '#type' => 'table',
      '#header' => [
        'product' => t('Product'),
        'quantity' => t('Sales'),
        'returned' => t('Return'),
        'amount' => t('Price'),
        'currency' => t('Currency'),
      ],
    ];
    $order = $payment->getOrder();
    $options = [];
    $refundSum = 0;
    $refunds = [];
    foreach ($order->getItems() as $orderItem) {
      $quantity = round($orderItem->getQuantity(), 2);
      $amount = $quantity;
      $price = $orderItem->getAdjustedUnitPrice()->getNumber();
      $currency = $orderItem->getAdjustedUnitPrice()->getCurrencyCode();
      $data = $orderItem->getData('refunded');
      if (!empty($data)) {
        foreach ($data as $key => $value) {
          $refunded = substr($value, 0, strpos($value, '||'));
          $amount -= $refunded;
          $time = substr($value, strpos($value, '||') + 2, strpos($value, '||', strpos($value, '||') + 1) - strpos($value, '||') - 2);
          $refunds[] = [
            'product' => $orderItem->getTitle(),
            'quantity' => $refunded,
            'date' => \Drupal::service('date.formatter')->format($time, 'short'),
            'bankId' => substr($value, strrpos($value, '||') + 2),
          ];
        }
      }
      $refundSum += $amount * $price;
      $form['orderItems'][$orderItem->id()]['product'] = [
        '#type' => 'label',
        '#title' => $orderItem->getTitle(),
      ];
      $form['orderItems'][$orderItem->id()]['quantity'] = [
        '#type' => 'label',
        '#title' => $quantity,
      ];
      $form['orderItems'][$orderItem->id()]['returned'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => $amount,
        '#step' => 1,
        '#size' => 1,
        '#default_value' => $amount,
        '#ajax' => [
          'callback' => '::updateRefundSum',
          'event' => 'change',
          'effect'   => 'none',
          'progress' => ['type' => 'throbber', 'message' => 'Минуточку...'],
        ],
      ];
      $form['orderItems'][$orderItem->id()]['amount'] = [
        '#type' => 'textfield',
        '#size' => 1,
        '#default_value' => $price,
        '#attributes' => ['readonly' => 'readonly'],
      ];
      $form['orderItems'][$orderItem->id()]['currency'] = [
        '#type' => 'label',
        '#title' => $currency,
      ];
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $price = $adjustment->getAmount()->getNumber();
      $amount = 1;
      $currency = $adjustment->getAmount()->getCurrencyCode();
      $data = $order->getData('refunded');
      if (!empty($data)) {
        foreach ($data as $key => $value) {
          $refunded = substr($value, 0, strpos($value, '||'));
          $amount -= $refunded;
          $time = substr($value, strpos($value, '||') + 2, strpos($value, '||', strpos($value, '||') + 1) - strpos($value, '||') - 2);
          $refunds[] = [
            'product' => $adjustment->getLabel(),
            'quantity' => $refunded,
            'date' => \Drupal::service('date.formatter')->format($time, 'short'),
            'bankId' => substr($value, strrpos($value, '||') + 2),
          ];
        }
      }
      $refundSum += $amount * $price;
      $form['orderItems']["adj_{$adjustment->getSourceId()}"]['product'] = [
        '#type' => 'label',
        '#title' => $adjustment->getLabel(),
      ];
      $form['orderItems']["adj_{$adjustment->getSourceId()}"]['quantity'] = [
        '#type' => 'label',
        '#title' => 1,
      ];
      $form['orderItems']["adj_{$adjustment->getSourceId()}"]['returned'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => $amount,
        '#step' => 1,
        '#size' => 1,
        '#default_value' => $amount,
        '#ajax' => [
          'callback' => '::updateRefundSum',
          'event' => 'change',
          'effect'   => 'none',
          'progress' => ['type' => 'throbber', 'message' => 'Минуточку...'],
        ],
      ];
      $form['orderItems']["adj_{$adjustment->getSourceId()}"]['amount'] = [
        '#type' => 'textfield',
        '#size' => 1,
        '#default_value' => round($price, 2),
        '#attributes' => ['readonly' => 'readonly'],
      ];
      $form['orderItems']["adj_{$adjustment->getSourceId()}"]['currency'] = [
        '#type' => 'label',
        '#title' => $currency,
      ];
    }
    $form['refunded_details'] = [
      '#type' => 'details',
      '#title' => 'Возвраты',
      'items' => [
        '#type' => 'table',
        '#header' => [
          'date' => t('Date'),
          'product' => t('Product'),
          'quantity' => t('Quantity'),
          'bankId' => t('Bank processing id'),
        ],
        '#empty' => t('Empty'),
      ],
    ];
    $time = array_column($refunds, 'date');
    array_multisort($time, SORT_ASC, $refunds);
    foreach ($refunds as $key => $refund) {
      $form['refunded_details']['items']['#rows'][$key] = [
        $refund['date'],
        $refund['product'],
        $refund['quantity'],
        $refund['bankId'],
      ];
    }
    $form['refundSum'] = [
      '#type' => 'textfield',
      '#title' => 'Сумма к возврату',
      '#size' => 3,
      '#attributes' => ['readonly' => 'readonly'],
      '#default_value' => $refundSum,
      '#suffix' => "<div id='refund-answer'></div>",
    ];
    $form['aсtions'] = [
      '#type' => 'actions',
      'get_status' => [
        '#type' => 'button',
        '#value' => $this->t('Return'),
        '#attributes' => ['class' => ['inline', 'btn-success']],
        '#ajax'   => [
          'callback' => '::ajaxRefund',
          'progress' => ['type' => 'throbber', 'message' => NULL],
        ],
      ],
      'update_status' => [],
    ];
    return $form;
  }

  /**
   * AJAX ajaxPrev.
   */
  public function updateRefundSum(array &$form, &$form_state) {
    $orderItems = $form_state->getValues()['orderItems'];
    $refundSum = 0;
    foreach ($orderItems as $key => $value) {
      $refundSum += $value['returned'] * $value['amount'];
    }
    $form['refundSum']['#value'] = $refundSum;
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand(".form-item--refundsum", $form['refundSum']));
    $form_state->setRebuild(TRUE);
    return $response;
  }

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxRefund(array &$form, &$form_state) {
    $response = new AjaxResponse();
    $values = $form_state->getValues();
    $orderId = $values['order_id'];
    $remoteId = $values['remote_id'];
    $manager = \Drupal::service('plugin.manager.synpay');
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($remoteId);
    $pluginId = $payment->getPaymentGateway()->getPlugin()->getConfiguration()['gateway'];
    $plugin = $manager->createInstance($pluginId, [
      'order' => $orderId,
    ]);
    $refundSum = 0;
    $items = [];
    foreach ($values['orderItems'] as $key => $value) {
      if ($value['returned'] > 0) {
        $refundSum += $value['returned'] * $value['amount'];
        $items[] = [
          'name' => $form['orderItems'][$key]['product']['#title'],
          'price' => round($value['amount'], 2),
          'quantity' => $value['returned'],
        ];
      }
    }
    $result = $plugin->requestRefund($payment, $refundSum, $items);
    if (isset($result['error'])) {
      $text = "Ошибка: {$result['error']} <br>";
      $text .= "{$result['text']}";
    }
    else {
      $item_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
      $time = time();
      foreach ($values['orderItems'] as $key => $value) {
        if ($value['returned'] > 0) {
          if (is_numeric($key)) {
            $item = $item_storage->load($key);
            $data = $item->getData('refunded');
            $data[] = "{$value['returned']}||$time||{$result['refund_id']}";
            $item->setData('refunded', $data)->save();
          }
          else {
            $order = $payment->getOrder();
            $data = $order->getData('refunded');
            $data[] = "{$value['returned']}||$time||{$result['refund_id']}";
            $order->setData('refunded', $data)->save();
          }
        }
      }
      // Determine whether payment has been fully or partially refunded.
      $old_refunded_amount = $payment->getRefundedAmount();
      $addSum = new Price($refundSum, $old_refunded_amount->getCurrencyCode());
      $new_refunded_amount = $old_refunded_amount->add($addSum);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }
      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
      $text = "Успешно вернули $refundSum";
    }
    $response->addCommand(new HtmlCommand("#refund-answer", $text));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
