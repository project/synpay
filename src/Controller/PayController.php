<?php

namespace Drupal\synpay\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for zakaz routes.
 */
class PayController extends ControllerBase {

  /**
   * Pay page.
   */
  public function page(string $plugin_name, string $order, string $total) {
    $manager = \Drupal::service('plugin.manager.synpay');
    $definitions = $manager->getDefinitions();
    if (array_key_exists($plugin_name, $definitions)) {
      $plugin = $manager->createInstance($plugin_name, [
        'order' => $order,
        'total' => floatval($total),
      ]);
      $data = [
        'plugin' => $plugin,
        'order' => $order,
        'total' => floatval($total),
      ];
      if ($plugin->active) {
        $form = \Drupal::formBuilder()->getForm('Drupal\synpay\Form\PayForm', $data);
        $build['form'] = $form;
        return $build;
      }
      \Drupal::messenger()->addWarning(t("Gateway does not active!"));
    }
    else {
      \Drupal::messenger()->addWarning(t("Gateway does not exist!"));
      \Drupal::messenger()->addWarning(t("Please check configuration."));
    }
    return [
      '#markup' => 'Error!',
    ];
  }

  /**
   * Test page.
   */
  public function test(string $plugin_name, $orderId) {
    $manager = \Drupal::service('plugin.manager.synpay');
    $pluginNames = $manager->getDefinitions();
    if (array_key_exists($plugin_name, $pluginNames)) {
      $text = "Вы используете $plugin_name";
      $plugin = $manager->createInstance($plugin_name, []);
      $order = \Drupal::EntityTypeManager()->getStorage('commerce_order')->load($orderId);
      $result = $plugin->onReturnTest($order);
    }
    else {
      $text = "$plugin_name не найден";
    }
    return [
      '#markup' => $text,
    ];
  }

  /**
   * Onsite plugin page.
   */
  public function onsite(string $plugin_name, $paymentId) {
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
      '@j', ['@j' => json_encode($paymentId ?? [])]
    );
    $manager = \Drupal::service('plugin.manager.synpay');
    $pluginNames = $manager->getDefinitions();
    // dsm($pluginNames);
    if (array_key_exists($plugin_name, $pluginNames)) {
      $text = "Вы используете $plugin_name";
      $plugin = $manager->createInstance($plugin_name, []);
      $result = $plugin->onsite($paymentId);
      // dsm($result);
      if (isset($result['error'])) {
        if (empty($result['url'])) {
          return [
            '#markup' => 'Ошибка!',
          ];
        }
        return new RedirectResponse($result['url']->toString());
      }
      return [
        '#markup' => $result['text'],
        '#attached' => [
          'drupalSettings' => [
            'synpay' => $result['data'],
          ],
          'library' => $result['library'],
        ],
      ];
    }
    else {
      $text = "$plugin_name не найден";
    }
    return [
      '#markup' => $text,
    ];
  }

  /**
   * Title Callback.
   */
  public function check(OrderInterface $commerce_order) {
    $form = \Drupal::formBuilder()->getForm('Drupal\synpay\Form\PaymentsCheckForm', $commerce_order);
    return $form;
  }

  /**
   * CallBack (for robots)
   */
  public function callback(string $plugin_name, Request $request) {
    $manager = \Drupal::service('plugin.manager.synpay');
    $plugin = $manager->createInstance($plugin_name, []);
    if ($plugin) {
      $order_status = $plugin->callback($request);
      // $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
      // $payment = $payment_storage->loadByRemoteId($order_status['order_id']);
      // $this->order = $order = Order::load($payment->getOrderId());
      // $step_id = $order->get('checkout_step')->getValue()[0]['value'];
      // $checkout_flow = $order->get('checkout_flow')->entity;
      // $checkout_flow_plugin = $checkout_flow->getPlugin();
      // switch ($order_status['status']) {
      //   case 'completed':
      //     $payment->setState('completed');
      //     $payment->setRemoteState('paid');
      //     $payment->save();
      //     $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);
      //     $this->redirectToStep($redirect_step_id);
      //     return new Response("OK{$order_status['order_id']}", 200);
      //
      //   default:
      //     $previous_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
      //     $this->redirectToStep($previous_step_id);
      //     return new Response("Error{$order_status['order_id']}", 406);
      // }
    }
    return $order_status;
  }

  /**
   * Return (for users) only for Robokassa.
   */
  public function return(string $plugin_name, Request $request) {
    $manager = \Drupal::service('plugin.manager.synpay');
    $plugin = $manager->createInstance($plugin_name, []);
    if ($plugin) {
      $order_status = $plugin->return($request);
      $redirect = "/checkout/{$order_status['order_id']}/";

      switch ($order_status['status']) {
        case 'completed':
          $redirect .= "complete";
          \Drupal::messenger()->addMessage($this->t("Payment completed successfully!"));
          break;

        default:
        case 'authorization_voided':
          $redirect .= "review";
          $message = $this->t("Payment not completed! Please contact support!");
          \Drupal::messenger()->addError($message);

      }
    }
    $response = new TrustedRedirectResponse($redirect);
    $response->send();
    return [
      "#markup" => '',
    ];
  }

  /**
   * Return (for direct payments).
   */
  public function returnDirect(string $plugin_name, Request $request) {
    $manager = \Drupal::service('plugin.manager.synpay');
    $plugin = $manager->createInstance($plugin_name, []);
    if ($plugin) {
      $order_status = $plugin->callback($request);
      switch ($order_status['status']) {
        case 'completed':
          $text = $this->t("Payment completed successfully!");
          // Return new Response("OK{$order_status['order_id']}", 200);.
          break;

        default:
          $text = $this->t("Payment not completed! Please contact support!");
          // Return new Response("Error{$order_status['order_id']}", 406);.
          break;
      }
    }
    return [
      "#markup" => $text,
    ];
  }

  /**
   * Redirect To Step.
   */
  public function redirectToStep($step_id) {
    $this->order->set('checkout_step', $step_id);
    if ($step_id == 'payment') {
      $this->order->lock();
    }
    elseif ($step_id != 'payment') {
      $this->order->unlock();
    }
    // Place the order.
    if ($step_id == 'complete' && $this->order->getState()->getId() == 'draft') {
      $this->order->getState()->applyTransitionById('place');
    }
    $this->order->save();
  }

  /**
   * Title Callback.
   */
  public function title() {
    return $this->t("Custom Pay");
  }

}
