<?php

namespace Drupal\synpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Synpay Payment Gateway.
 *
 * @CommercePaymentGateway(
 *   id = "synpay",
 *   label = @Translation("Synpay Gateway"),
 *   display_label = @Translation("Synpay Gateway"),
 *   forms = {
 *     "offsite-payment" = "Drupal\synpay\PluginForm\OffsiteRedirect\OffsitePaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   requires_billing_information = FALSE,
 *   credit_card_types = {
 *     "maestro", "mastercard", "visa", "mir",
 *   },
 * )
 *     "refund-payment"  = "Drupal\synpay\PluginForm\PaymentRefundForm",
 *     "view-refund"  = "Drupal\synpay\PluginForm\ListRefundForm",
 */
class SynpayPaymentGateway extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = \Drupal::config('synpay.settings');
    $manager = \Drupal::service('plugin.manager.synpay');
    $form['gateway'] = [
      '#title' => $this->t("Gateway"),
      '#type' => 'radios',
      '#required' => TRUE,
      '#default_value' => $this->configuration['gateway'] ?? '',
    ];
    foreach ($manager->getDefinitions() as $gateway => $source) {
      $active = $config->get("{$gateway}_active") ?? 0;
      if ($active) {
        $form['gateway']['#options'][$gateway] = $source['title']->getUntranslatedString();
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    if (empty($values['gateway'])) {
      $form_state->setError($form['gateway'], $this->t("Gateway is required."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['gateway'] = $values['gateway'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_gateway_plugin = $this->parentEntity->getPlugin();
    $configs = $payment_gateway_plugin->getConfiguration();
    $synpay = \Drupal::service('synpay.gateway')
      ->init($configs['mode'], $configs);
    $order_status = $synpay->onReturn($order, $request);
    if ($order_status != 'completed') {
      $message = $this->t("Payment failed!");
      \Drupal::messenger()->addError($message);
      throw new PaymentGatewayException('');
    }
  }

  /**
   * Processes the notification request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The response, or NULL to return an empty HTTP 200 response.
   */
  public function onNotify(Request $request) {
    $payment_gateway_plugin = $this->parentEntity->getPlugin();
    $configs = $payment_gateway_plugin->getConfiguration();
    $synpay = \Drupal::service('synpay.gateway')
      ->init($configs['mode'], $configs);
    $response = $synpay->onNotify($request);
    if ($response) {
      return $response;
    }
    return new Response('OK', 200);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $payment_state = $payment->getState()->getId();
    $operations = [];
    // $operations['refund'] = [
    //   'title'       => $this->t('Refund'),
    //   'page_title'  => $this->t('Refund payment'),
    //   'plugin_form' => 'refund-payment',
    //   'access'      => $this->canRefundPayment($payment),
    // ];
    // $operations['view_refund'] = [
    //   'title'       => $this->t('View refund'),
    //   'page_title'  => $this->t('View refund Details'),
    //   'plugin_form' => 'view-refund',
    //   'access'      => in_array($payment_state, ['partially_refunded', 'refunded']),
    // ];
    return $operations;
  }

}
