<?php

namespace Drupal\synpay\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Block for Synpay Dolyame.
 *
 * @Block(
 *   id = "modal_mydolyame",
 *   admin_label = @Translation("Dolyame"),
 * )
 */
class ModalDolyame extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [
      '#theme' => 'block__modal_mydolyame',
    ];
    return $block;
  }

}
