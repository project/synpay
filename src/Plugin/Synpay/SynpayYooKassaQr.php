<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;

/**
 * Provides a Ykassa Gateway.
 *
 * @SynpayAnnotation(
 *   id = "yookassa_qr",
 *   title = @Translation("Ykassa Qr"),
 * )
 */
class SynpayYooKassaQr extends SynpayYooKassa implements SynpayPluginInterface {
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->payment_type = 'sbp';
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    $form["{$this->plugin_id}_send_receipt"] = [
      '#type' => 'checkbox',
      '#title' => 'Передавать данные для формирования чека',
      '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    ];
    $form['column']["{$this->plugin_id}_taxation"] = [
      '#type' => 'select',
      '#title' => 'Система налогообложения',
      '#options' => [
          1 => 'Общая система налогообложения',
          2 => 'Упрощенная (УСН, доходы)',
          3 => 'Упрощенная (УСН, доходы минус расходы)',
          4 => 'Единый налог на вмененный доход (ЕНВД)',
          5 => 'Единый сельскохозяйственный налог (ЕСН)',
          6 => 'Патентная система налогообложения',
      ],
      '#default_value' =>  $config->get("{$this->plugin_id}_taxation"),
    ];
    $form["{$this->plugin_id}_tax"] = [
      '#type' => 'select',
      '#title' => 'Ставка',
      '#options' => [
          1 => 'Без НДС',
          2 => '0%',
          3 => '10%',
          4 => '20%',
          5 => 'Расчётная ставка 10/110',
          6 => 'Расчётная ставка 20/120',
      ],
      '#default_value' =>  $config->get("{$this->plugin_id}_tax"),
    ];
    $form["{$this->plugin_id}_payment_method"] = [
      '#type' => 'select',
      '#title' => 'Способ расчета',
      '#options' => [
        'full_prepayment' => 'Полная предоплата (full_prepayment)',
        'partial_prepayment' => 'Частичная предоплата (partial_prepayment)',
        'advance' => 'Аванс (advance)',
        'full_payment' => 'Полный расчет (full_payment)',
        'partial_payment' => 'Частичный расчет и кредит (partial_payment)',
        'credit' => 'Кредит (credit)',
        'credit_payment' => 'Выплата по кредиту (credit_payment)',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_method"),
    ];
    $form["{$this->plugin_id}_payment_object"] = [
      '#type' => 'select',
      '#title' => 'Предмет расчета',
      '#options' => [
        'commodity' => 'Товар (commodity)',
        'excise' => 'Подакцизный товар (excise)',
        'job' => 'Работа (job)',
        'service' => 'Услуга (service)',
        'gambling_bet' => 'Ставка в азартной игре (gambling_bet)',
        'gambling_prize' => 'Выигрыш в азартной игре (gambling_prize)',
        'lottery' => 'Лотерейный билет (lottery)',
        'lottery_prize' => 'Выигрыш в лотерею (lottery_prize)',
        'intellectual_activity' => 'Результаты интеллектуальной деятельности (intellectual_activity)',
        'payment' => 'Платеж (payment)',
        'agent_commission' => 'Агентское вознаграждение (agent_commission)',
        'composite' => 'Несколько вариантов (composite)',
        'another' => 'Другое (another)',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_object"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayYandex')->info($message);
  }

}
