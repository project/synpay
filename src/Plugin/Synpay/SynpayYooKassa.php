<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use YooKassa\Client;
use YooKassa\Common\Exceptions\NotFoundException;

/**
 * Provides a Ykassa Gateway.
 *
 * @SynpayAnnotation(
 *   id = "yookassa",
 *   title = @Translation("Ykassa"),
 * )
 */
class SynpayYooKassa extends SynpayPluginBase implements SynpayPluginInterface {
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->payment_type = 'bank_card';
    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->receipt = $this->config->get("{$plugin_id}_send_receipt");
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = new Client();
    $this->client->setAuth(trim($this->user ?? ''), trim($this->pass ?? ''));
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $return = "$host/synpay/return_direct/yookassa";
    $order = Uuid::uuid4()->getHex()->toString();
    try {
      $response = $this->client->createPayment([
        'amount' => [
          'value' => $total,
          'currency' => 'RUB',
        ],
        'payment_method_data' => [
          'type' => $this->payment_type,
        ],
        'confirmation' => [
          'type' => 'redirect',
          'return_url' => $return,
        ],
        'capture' => TRUE,
        'description' => "Заказ №$order",
      ],
      uniqid('', TRUE),
      );
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::logger('synpay_yookassa')->error($message);
      return FALSE;
    }
    $url = $response->getConfirmation()->getConfirmationUrl();
    $orderId = substr($url, strpos($url, 'orderId=') + 8);
    $session = \Drupal::request()->getSession();
    $session->set('orderId', $orderId);
    $redirect = new TrustedRedirectResponse($response->getConfirmation()->getConfirmationUrl());
    $redirect->send();
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    if (is_null($this->client)) {
      return FALSE;
    }
    $data = [
      'amount' => [
        'value' => $order_amount,
        'currency' => 'RUB',
      ],
      'payment_method_data' => [
        'type' => $this->payment_type,
      ],
      'confirmation' => [
        'type' => 'redirect',
        'return_url' => $params['returnUrl'],
      ],
      'capture' => TRUE,
      'description' => "Заказ №$order_id",
    ];
    if ($this->receipt) {
      $order = $payment->getOrder();
      $data['receipt'] = [
        'customer' => $this->getCustomer($order),
        'items' => $this->getItems($order),
      ];
    }
    try {
      $response = $this->client->createPayment($data, uniqid('', TRUE));
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::logger('synpay_yookassa')->error($message);
      return FALSE;
    }
    return [
      'orderId' => $response->getId(),
      'formUrl' => $response->getConfirmation()->getConfirmationUrl(),
    ];
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  private function getCustomer(OrderInterface $order) {
    $profile = $order->getBillingProfile();
    $name = $profile->field_customer_name->value ?? '';
    $surname = $profile->field_customer_surname->value ?? '';
    $email = $profile->field_customer_email->value ?? '';
    $phone = $profile->field_customer_phone->value ?? '';
    $customer = [
      'full_name' => "$name $surname",
      'email' => $email,
      'phone' => str_replace(['(', ')', '-', ' '], '', $phone),
      'inn' => '',
    ];
    return $customer;
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  private function getItems(OrderInterface $order) {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $total = (int) $item->getUnitPrice()->getNumber();
      $total = self::PRECISION ? $total : bcmul($total, 100, 0);
      $currency = $item->getUnitPrice()->getCurrencyCode();
      $items[$c] = [
        'description' => $item->getTitle(),
        'quantity' => $item->getQuantity(),
        'amount' => [
          'value' => $total,
          'currency' => $currency,
        ],
        'vat_code' => $this->config->get("{$this->plugin_id}_tax"),
        'payment_mode' => $this->config->get("{$this->plugin_id}_payment_method"),
        'payment_subject' => $this->config->get("{$this->plugin_id}_payment_object"),
      ];
      $c++;
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $amount = $adjustment->getAmount()->getNumber();
      if ($amount == 0) {
        continue;
      }
      $amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
      $currency = $adjustment->getAmount()->getCurrencyCode();
      $items[] = [
        'description' => $adjustment->getLabel(),
        'quantity' => 1,
        'amount' => [
          'value' => $amount,
          'currency' => $currency,
        ],
        'vat_code' => $this->config->get("{$this->plugin_id}_tax"),
        'payment_mode' => $this->config->get("{$this->plugin_id}_payment_method"),
      ];
      switch ($adjustment->getType()) {
        case 'shipping':
          $items[$c]['payment_subject'] = 'service';
          break;
      }
      $c++;
    }
    return $items;
  }

  /**
   * Pay callback.
   */
  public function callback(Request $request) {
    $session = \Drupal::request()->getSession();
    $orderId = $session->get('orderId');
    $payment = $this->client->getPaymentInfo($orderId);
    switch ($payment->getStatus()) {
      case 'succeeded':
        $result['status'] = 'completed';
        break;

      default:
        $result['status'] = 'any';
        break;
    }
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    $success = FALSE;
    foreach ($payments as $payment) {
      if (!empty($payment->remote_id->value)) {
        $status = $this->checkPayment($payment->remote_id->value);
        $answer = $this->updatePayment($payment, $status['response']);
        if ($answer == 'completed' && !$success) {
          $success = TRUE;
        }
      }
    }
    $result = $success ? 'completed' : $answer;
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $rawBody = $request->getContent();
    $this->log('Notification: ' . $rawBody);
    $paymentInfo = Json::decode($rawBody);
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $paymentInfo['object']['id']]);
    $payment = reset($payments);
    if (!$payment) {
      return new Response('Bad request', 400);
    }
    $order = $payment->getOrder();
    if (!$order) {
      return new Response('Order not found', 404);
    }
    if ($paymentInfo['object']['paid']) {
      $payment->state = 'completed';
      $payment->setCompletedTime(time());
      if ($order->state->value != 'completed') {
        $event = new OrderEvent($order);
        $eventDispatcher = \Drupal::service('event_dispatcher');
        $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
        $order->getState()->applyTransitionById('place');
        $order->save();
      }
    }
    else {
      $payment->state = 'authorization_voided';
    }
    $payment->setRemoteState($paymentInfo['object']['status']);
    $payment->save();

    return new Response('OK', 200);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if (!$answer['error']) {
      // If (\Drupal::currentUser()->id() == 1) {
      //   dsm($answer['response']);
      // }.
      $answer['status'] = $answer['response']->getStatus();
    }
    else {
      $answer['status'] = $answer['response'];
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if (!$answer['error']) {
      // If (\Drupal::currentUser()->id() == 1) {
      //   dsm($answer['response']);
      // }.
      $answer['status'] = $answer['response']->getStatus();
      $this->updatePayment($remote_id, $answer);
    }
    else {
      $answer['status'] = $answer['response'];
    }
    return $answer;
  }

  /**
   * Check Payments.
   */
  private function checkPayment($id) {
    $result = [];
    try {
      $result['error'] = FALSE;
      $result['response'] = $this->client->getPaymentInfo($id);
    }
    catch (NotFoundException $exception) {
      $result['error'] = TRUE;
      $result['response'] = $exception->getMessage();
      \Drupal::logger('synpay_yookassa')->error($exception->getMessage());
    }
    return $result;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    elseif ($status->getPaid()) {
      $payment->state = 'completed';
      $payment->setCompletedTime(time());
      $payment->setRemoteState($status->getStatus());
      $result = 'completed';
    }
    else {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState($status->getStatus());
      $result = 'authorization_voided';
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    $form["{$this->plugin_id}_send_receipt"] = [
      '#type' => 'checkbox',
      '#title' => 'Передавать данные для формирования чека',
      '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    ];
    $form['column']["{$this->plugin_id}_taxation"] = [
      '#type' => 'select',
      '#title' => 'Система налогообложения',
      '#options' => [
        1 => 'Общая система налогообложения',
        2 => 'Упрощенная (УСН, доходы)',
        3 => 'Упрощенная (УСН, доходы минус расходы)',
        4 => 'Единый налог на вмененный доход (ЕНВД)',
        5 => 'Единый сельскохозяйственный налог (ЕСН)',
        6 => 'Патентная система налогообложения',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_taxation"),
    ];
    $form["{$this->plugin_id}_tax"] = [
      '#type' => 'select',
      '#title' => 'Ставка',
      '#options' => [
        1 => 'Без НДС',
        2 => '0%',
        3 => '10%',
        4 => '20%',
        5 => 'Расчётная ставка 10/110',
        6 => 'Расчётная ставка 20/120',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_tax"),
    ];
    $form["{$this->plugin_id}_payment_method"] = [
      '#type' => 'select',
      '#title' => 'Способ расчета',
      '#options' => [
        'full_payment' => 'Полный расчет (full_payment)',
        'full_prepayment' => 'Полная предоплата (full_prepayment)',
        'partial_prepayment' => 'Частичная предоплата (partial_prepayment)',
        'advance' => 'Аванс (advance)',
        'partial_payment' => 'Частичный расчет и кредит (partial_payment)',
        'credit' => 'Кредит (credit)',
        'credit_payment' => 'Выплата по кредиту (credit_payment)',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_method"),
    ];
    $form["{$this->plugin_id}_payment_object"] = [
      '#type' => 'select',
      '#title' => 'Предмет расчета',
      '#options' => [
        'commodity' => 'Товар (commodity)',
        'excise' => 'Подакцизный товар (excise)',
        'job' => 'Работа (job)',
        'service' => 'Услуга (service)',
        'gambling_bet' => 'Ставка в азартной игре (gambling_bet)',
        'gambling_prize' => 'Выигрыш в азартной игре (gambling_prize)',
        'lottery' => 'Лотерейный билет (lottery)',
        'lottery_prize' => 'Выигрыш в лотерею (lottery_prize)',
        'intellectual_activity' => 'Результаты интеллектуальной деятельности (intellectual_activity)',
        'payment' => 'Платеж (payment)',
        'agent_commission' => 'Агентское вознаграждение (agent_commission)',
        'composite' => 'Несколько вариантов (composite)',
        'another' => 'Другое (another)',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_object"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayYandex')->info($message);
  }

}
