<?php

namespace Drupal\synpay\Plugin\Synpay;

use Ramsey\Uuid\Uuid;
use GuzzleHttp\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Symfony\Component\HttpFoundation\Request;
use Voronkovich\SberbankAcquiring\OrderStatus;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Voronkovich\SberbankAcquiring\Client as SberbankClient;
use Voronkovich\SberbankAcquiring\Exception\ActionException;
use Voronkovich\SberbankAcquiring\HttpClient\GuzzleAdapter as SberbankGuzzleAdapter;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Provides a 'Sbercredit' PayPlugin.
 *
 * Visa:
 * Номер карты 4111 1111 1111 1111
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=y, pares=y
 * Проверочный код 3-D Secure 12345678
 *
 * MasterCard:
 * Номер карты 5555 5555 5555 5599
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=n
 *
 * Мир:
 * Номер карты 2200 0000 0000 0053
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=n
 * Проверочный код 3-D Secure 12345678
 *
 * @SynpayAnnotation(
 *   id = "sbercredit",
 *   title = @Translation("Sberbank credit"),
 * )
 */
class SynpaySbercredit extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Сбербанк эквайринг"';
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->currency = Currency::RUB;
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $api = SberbankClient::API_URI_TEST;
              break;

            case 'live':
              $api = SberbankClient::API_URI;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = new SberbankClient([
      'userName' => trim($this->user ?? ''),
      'password' => trim($this->pass ?? ''),
      'language' => $this->lang,
      // ISO 4217 currency code.
      'currency' => $this->currency,
      'apiUri' => $api ?? '',
      'prefixDefault' => '/sbercredit/',
      'httpClient' => new SberbankGuzzleAdapter(new Client()),
    ]);
    $this->clientGetStatus = new SberbankClient([
      'userName' => trim($this->user ?? ''),
      'password' => trim($this->pass ?? ''),
      'language' => $this->lang,
      'currency' => $this->currency,
      'apiUri' => $api ?? '',
      'httpClient' => new SberbankGuzzleAdapter(new Client()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $return = "$host/synpay/return_direct/{$this->plugin_id}";
    $params = [
      'failUrl' => "$host/synpay/return_direct/{$this->plugin_id}?fail",
    ];
    $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = bcmul($total, 100, 0);
    try {
      $result = $this->client->registerOrder($order, $order_amount, $return, $params);
      $redirect = $result['formUrl'];
      $response = new TrustedRedirectResponse($redirect);
      $response->send();
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    // Execute request to Sberbank.
    if (is_null($this->client)) {
      return FALSE;
    }
    try {
      $result = $this->client->registerOrder($order_id, $order_amount, $params['returnUrl'], $params);
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
      \Drupal::logger("synpay_{$this->plugin_id}")->error($message);
      return FALSE;
    }
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(string $remote_id) {
    $result = $this->clientGetStatus->getOrderStatus($remote_id);
    // Проверить.
    $result['order_id'] = $remote_id;
    switch ($result['orderStatus']) {
      case OrderStatus::DEPOSITED:
        $result['status'] = 'completed';
        break;

      default:
      case OrderStatus::DECLINED:
        $result['status'] = 'authorization_voided';
    }
    return $result;
  }

  /**
   * Pay callback.
   */
  public function callback(Request $request) {
    $remote_id = $request->get('mdOrder');
    $result = $this->onReturn($remote_id);
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(string $remote_id) {
    $result = $this->client->getOrderStatus($remote_id);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "Карта для использования:<br>
            Номер: 4111 1111 1111 1111, Срок действия: 12/24, CVC2/CVV2 код: 123, Пароль: 12345678";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    return $form;
  }

}
