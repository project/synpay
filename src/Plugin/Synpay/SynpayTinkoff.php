<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use GuzzleHttp\Exception\ClientException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides Tinkoff Gateway.
 *
 * @SynpayAnnotation(
 *   id = "tinkoff",
 *   title = @Translation("Tinkoff"),
 * )
 */
class SynpayTinkoff extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Тинькофф эквайринг"';
  const HOST = "https://securepay.tinkoff.ru/v2/";
  const HOST_TEST = "https://securepay.tinkoff.ru/v2/";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    $this->payment_object = $this->config->get("{$plugin_id}_payment_object");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $this->api = self::HOST_TEST;
              break;

            case 'live':
              $this->api = self::HOST;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = \Drupal::httpClient([
      'timeout'  => 10.0,
    ]);
    $this->headers = [
      'Content-type' => 'application/x-www-form-urlencoded',
      'Cache-Control' => 'no-cache',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = bcmul($total, 100, 0);
    $params = [
      'TerminalKey' => trim($this->user),
      'Amount' => $order_amount,
      'OrderId' => $order,
      'SuccessURL' => "{$this->host}/synpay/return_direct/$this->plugin_id",
      'FailURL' => "{$this->host}/synpay/return_direct/$this->plugin_id?fail",
    ];
    $this->addToken($params);
    $params['headers'] = $this->headers;
    try {
      $response = $this->client->post("{$this->api}Init", ['json' => $params]);
      $code = $response->getStatusCode();
      $result = Json::decode($response->getBody()->getContents());
    }
    catch (ClientException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    if (is_null($this->client)) {
      return FALSE;
    }
    $order = $payment->getOrder();
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    $terminal_key = trim($this->user);
    $data = [
      'TerminalKey' => $terminal_key,
      'Amount' => $order_amount,
      'OrderId' => $order_id,
      'Description' => "Заказ №{$order_id}",
      'Language' => $this->lang,
      'NotificationURL' => "{$this->host}/payment/notify/{$payment->getPaymentGatewayId()}",
      'SuccessURL' => $params['returnUrl'],
      'FailURL' => $params['failUrl'],
      'Receipt' => [
        'Email' => $order->getEmail(),
        'Taxation' => $this->config->get("{$this->plugin_id}_taxation"),
        'Items' => $this->getItems($order),
      ],
    ];
    $this->addToken($data);
    $data['headers'] = $this->headers;
    try {
      $answer = $this->client->post("{$this->api}Init", ['json' => $data]);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        if ($response['Success']) {
          return $this->registerOrderResponse($response);
        }
        else {
          \Drupal::messenger()->addError($response['Message']);
          \Drupal::messenger()->addError($response['Details']);
          return FALSE;
        }
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
    return FALSE;
  }

  /**
   * Добавление токена в параметры запроса.
   */
  protected function addToken(array &$data) {
    $token = $this->getToken($data);
    $data['Token'] = $token;
  }

  /**
   * Формирование токена для подписи запроса.
   */
  protected function getToken(array $data) : string {
    unset($data['Receipt']);
    unset($data['DATA']);
    $data['Password'] = trim($this->pass);
    ksort($data);
    return hash('sha256', implode('', $data));
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  protected function getItems(OrderInterface $order) {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = self::PRECISION ? $price : bcmul($price, 100, 0);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = self::PRECISION ? $total : bcmul($total, 100, 0);
      $items[$c] = [
        'Name' => $item->getTitle(),
        'Price' => $price,
        'Quantity' => $item->getQuantity(),
        'Amount' => $total,
        'PaymentObject' => $this->payment_object,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      $c++;
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $amount = $adjustment->getAmount()->getNumber();
      $amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
      $items[$c] = [
        'Name' => $adjustment->getLabel(),
        'Price' => $amount,
        'Quantity' => 1,
        'Amount' => $amount,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      switch ($adjustment->getType()) {
        case 'shipping':
          $items[$c]['PaymentObject'] = 'service';
          break;
      }
      $c++;
    }
    return $items;
  }

  /**
   * Обработчик ответа на запрос регистрации заказа.
   */
  public function registerOrderResponse(array $response) : array {
    return [
      'orderId' => $response['PaymentId'],
      'formUrl' => $response['PaymentURL'],
    ];
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_lock_name = $this->acquirePaymentLockName($order->id());
    $lock = \Drupal::lock();
    if (!$lock->acquire($payment_lock_name, 9.0)) {
      $lock->wait($payment_lock_name, 9.0);
      $lock->acquire($payment_lock_name, 9.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    $success = FALSE;
    foreach ($payments as $payment) {
      $status = $this->checkPayment($payment->remote_id->value);
      $answer = $this->updatePayment($payment, $status);
      if ($answer == 'completed' && !$success) {
        $success = TRUE;
      }
    }
    $result = $success ? 'completed' : $answer;
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $paymentInfo = Json::decode(
      file_get_contents('php://input')
    );
    $lock = \Drupal::lock();
    $orderId = strstr($paymentInfo['OrderId'], '/', TRUE);
    $payment_lock_name = $this->acquirePaymentLockName($orderId);
    if (!$lock->acquire($payment_lock_name, 7.0)) {
      $lock->wait($payment_lock_name, 7.0);
      $lock->acquire($payment_lock_name, 7.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $paymentInfo['PaymentId']]);
    $payment = reset($payments);
    if (!$payment) {
      return new Response('Bad request', 400);
    }
    switch ($paymentInfo['Status']) {
      case 'AUTHORIZED':
        $payment->setState('completed');
        $payment->setCompletedTime(time());
        break;

      case 'REJECTED':
        $payment->setState('authorization_voided');
        break;
    }
    $payment->setRemoteState($paymentInfo['Status']);
    $payment->save();

    return new Response('OK', 200);
  }

  /**
   * {@inheritdoc}
   */
  protected function acquirePaymentLockName(string $orderId) : string {
    return sprintf('payment-lock-name-%d', $orderId);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
      $this->updatePayment($remote_id, $answer);
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Check Payments.
   */
  private function checkPayment($id) {
    $data = [
      'Password' => trim($this->pass),
      'PaymentId' => $id,
      'TerminalKey' => trim($this->user),
    ];
    // array_multisort($data, SORT_DESC, SORT_STRING, $data);.
    $token = '';
    foreach ($data as $value) {
      $token .= $value;
    }
    $hash = hash('sha256', $token);
    $params = [
      'headers' => $this->headers,
      'TerminalKey' => trim($this->user),
      'PaymentId' => $id,
      'Token' => $hash,
    ];
    try {
      $response = $this->client->post("{$this->api}GetState", ['json' => $params]);
      $code = $response->getStatusCode();
      $answer = Json::decode($response->getBody()->getContents());
      return $answer;
    }
    catch (ClientException $exception) {
      // $message = $exception->getMessage();
      // \Drupal::messenger()->addError($message);
    }
    return FALSE;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    else {
      switch ($status['Status']) {
        case 'CONFIRMED':
        case 'AUTHORIZED':
          $result = 'completed';
          break;

        default:
          $result = 'authorization_voided';
          break;
      }
      $payment->setState($result);
      $payment->setRemoteState($status['Status']);
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "Карта для \"Успешного платежа\":<br>
            Номер: 4300 0000 0000 0777, Срок действия: 11/22, CVC2/CVV2 код: любой<br>
            Карта для \"Неуспешного платежа\":<br>
            Номер: 5000 0000 0000 0009, Срок действия: 11/22, CVC2/CVV2 код: любой";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    // $form["{$this->plugin_id}_send_receipt"] = [
    //   '#type' => 'select',
    //   '#title' => 'Передавать данные для формирования чека',
    //   '#options' => [
    //       'no' => 'Нет',
    //       'yes' => 'Да'
    //   ],
    //   '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    // ];
    $form["{$this->plugin_id}_taxation"] = [
      '#type' => 'select',
      '#title' => 'Система налогообложения',
      '#options' => [
        'osn' => 'Общая',
        'usn_income' => 'Упрощенная (доходы)',
        'usn_income_outcome' => 'Упрощенная (доходы минус расходы)',
        'patent' => 'Патентная',
        'envd' => 'Единый налог на вмененный доход',
        'esn' => 'Единый сельскохозяйственный налог',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_taxation"),
    ];
    $form["{$this->plugin_id}_tax"] = [
      '#type' => 'select',
      '#title' => 'Ставка НДС',
      '#options' => [
        'none' => 'без НДС',
        'vat0' => '0%',
        'vat10' => '10%',
        'vat20' => '20%',
        'vat110' => '10/110',
        'vat120' => '20/120',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_tax"),
    ];
    $form["{$this->plugin_id}_payment_object"] = [
      '#type' => 'select',
      '#title' => 'Признак предмета расчёта',
      '#options' => [
        'commodity' => 'товар',
        'excise' => 'подакцизный товар',
        'job' => 'работа',
        'service' => 'услуга',
        'gambling_bet' => 'ставка азартной игры',
        'gambling_prize' => 'выигрыш азартной игры',
        'lottery' => 'лотерейный билет',
        'lottery_prize' => 'выигрыш лотереи',
        'intellectual_activity' => 'предоставление РИД',
        'payment' => 'платёж',
        'agent_commission' => 'агентское вознаграждение',
        'composite' => 'составной предмет расчёта',
        'another' => 'иной предмет расчёта',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_object"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayTinkoff')->info($message);
  }

}
