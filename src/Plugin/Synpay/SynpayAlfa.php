<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides Alfa Gateway.
 *
 * @SynpayAnnotation(
 *   id = "alfa",
 *   title = @Translation("alfa"),
 * )
 */
class SynpayAlfa extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Альфа эквайринг"';
  const HOST = "https://payment.alfabank.ru/payment/";
  const HOST_TEST = "https://web.rbsuat.com/ab/";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;
  const ORDERSTATUS = [
    0 => 'Заказ зарегистрирован, но не оплачен',
    1 => 'Предавторизованная сумма захолдирована',
    2 => 'Проведена полная авторизация суммы заказа',
    3 => 'Авторизация отменена',
    4 => 'По транзакции была проведена операция возврата',
    5 => 'Инициирована авторизация через ACS банка-эмитента',
    6 => 'Авторизация отклонена',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $this->api = self::HOST_TEST;
              break;

            case 'live':
              $this->api = self::HOST;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = \Drupal::httpClient([
      'timeout'  => 10.0,
    ]);
    $this->headers = [
      'Content-type' => 'application/x-www-form-urlencoded',
      'Cache-Control' => 'no-cache',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = bcmul($total, 100, 0);
    $params = [
      // 'headers' => $this->headers,
      'userName' => trim($this->user),
      'password' => trim($this->pass),
      'orderNumber' => $order,
      'amount' => $order_amount,
      'returnUrl' => "{$this->host}/synpay/return_direct/$this->plugin_id",
      'failUrl' => "{$this->host}/synpay/return_direct/$this->plugin_id?fail",
    ];
    $url = "{$this->api}rest/register.do";
    try {
      $response = $this->client->post($url, ['form_params' => $params]);
      $code = $response->getStatusCode();
      $result = Json::decode($response->getBody()->getContents());
    }
    catch (ClientException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    if (is_null($this->client)) {
      return FALSE;
    }
    $order = $payment->getOrder();
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    $data = [
      'userName' => trim($this->user),
      'password' => trim($this->pass),
      'orderNumber' => $order_id,
      'amount' => $order_amount,
      'returnUrl' => $params['returnUrl'],
      'failUrl' => $params['failUrl'],
      'language' => $this->lang,
      // 'dynamicCallbackUrl' => "{$this->host}/payment/notify/{$payment->getPaymentGatewayId()}",
      // 'Receipt' => [
      //   'Email' => $order->getEmail(),
      //   'Taxation' => $this->config->get("{$this->plugin_id}_taxation"),
      //   'Items' => $this->getItems($order),
      // ],
    ];
    try {
      $answer = $this->client->post("{$this->api}rest/register.do", ['form_params' => $data]);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        if (!array_key_exists('errorCode', $response)) {
          return [
            'orderId' => $response['orderId'],
            'formUrl' => $response['formUrl'],
          ];
        }
        else {
          \Drupal::messenger()->addError($response['errorCode']);
          \Drupal::messenger()->addError($response['errorMessage']);
          return FALSE;
        }
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
    return FALSE;
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  private function getItems(OrderInterface $order) {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = self::PRECISION ? $price : bcmul($price, 100, 0);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = self::PRECISION ? $total : bcmul($total, 100, 0);
      $items[$c] = [
        'Name' => $item->getTitle(),
        'Price' => $price,
        'Quantity' => $item->getQuantity(),
        'Amount' => $total,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      $c++;
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $amount = $adjustment->getAmount()->getNumber();
      $amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
      $items[$c] = [
        'Name' => $adjustment->getLabel(),
        'Price' => $amount,
        'Quantity' => 1,
        'Amount' => $amount,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      switch ($adjustment->getType()) {
        case 'shipping':
          $items[$c]['PaymentObject'] = 'service';
          break;
      }
      $c++;
    }
    return $items;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_lock_name = $this->acquirePaymentLockName($order->id());
    $lock = \Drupal::lock();
    if (!$lock->acquire($payment_lock_name, 8.0)) {
      $lock->wait($payment_lock_name, 8.0);
      $lock->acquire($payment_lock_name, 8.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    $success = FALSE;
    foreach ($payments as $payment) {
      $status = $this->checkPayment($payment->remote_id->value);
      $answer = $this->updatePayment($payment, $status);
      if ($answer == 'completed' && !$success) {
        $success = TRUE;
      }
    }
    $result = $success ? 'completed' : $answer;
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $paymentInfo = Json::decode(
      file_get_contents('php://input')
    );
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
      '@j', ['@j' => json_encode($paymentInfo ?? [])]
    );
    $lock = \Drupal::lock();
    $orderId = strstr($paymentInfo['OrderId'], '/', TRUE);
    $payment_lock_name = $this->acquirePaymentLockName($orderId);
    if (!$lock->acquire($payment_lock_name, 8.0)) {
      $lock->wait($payment_lock_name, 8.0);
      $lock->acquire($payment_lock_name, 8.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $paymentInfo['PaymentId']]);
    $payment = reset($payments);
    if (!$payment) {
      return new Response('Bad request', 400);
    }
    switch ($paymentInfo['Status']) {
      case 'AUTHORIZED':
        $payment->setState('completed');
        $payment->setCompletedTime(time());
        break;

      case 'REJECTED':
        $payment->setState('authorization_voided');
        break;
    }
    $payment->setRemoteState($paymentInfo['Status']);
    $payment->save();

    return new Response('OK', 200);
  }

  /**
   * {@inheritdoc}
   */
  private function acquirePaymentLockName(string $orderId) : string {
    return sprintf('payment-lock-name-%d', $orderId);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
      $this->updatePayment($remote_id, $answer);
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Check Payments.
   */
  private function checkPayment($id) {
    $params = [
      'userName' => trim($this->user),
      'password' => trim($this->pass),
      'orderId' => $id,
    ];
    try {
      $response = $this->client->post("{$this->api}rest/getOrderStatus.do", ['form_params' => $params]);
      $code = $response->getStatusCode();
      $answer = Json::decode($response->getBody()->getContents());
      return $answer;
    }
    catch (ClientException $exception) {
      // $message = $exception->getMessage();
      // \Drupal::messenger()->addError($message);
    }
    return FALSE;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    else {
      switch ($status['OrderStatus']) {
        case 2:
          $result = 'completed';
          break;

        default:
          $result = 'authorization_voided';
          break;
      }
      $payment->setState($result);
      $payment->setRemoteState(self::ORDERSTATUS[$status['OrderStatus']]);
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "Карта для \"Успешного платежа\":<br>
            Номер: 4111 1111 1111 1111, Имя держателя карты: SUCCESSPAYMENT, Срок действия: 12/24, CVC2/CVV2 код: 123, Код подтверждения: 12345678<br>
            Карта для \"Неуспешного платежа\":<br>
            Номер: 4111 1111 1111 1111, Имя держателя карты: ERRORPAYMENT, Срок действия: 12/24, CVC2/CVV2 код: 111, Код подтверждения: 12345678";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый пароль $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий пароль $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayAlfa')->info($message);
  }

}
