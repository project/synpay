<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_payment\Entity\PaymentInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides PayKeeper Gateway.
 *
 * @SynpayAnnotation(
 *   id = "paykeeper_qr",
 *   title = @Translation("Pay Keeper Qr"),
 * )
 */
class SynpayPayKeeperQr extends SynpayPayKeeper {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->QR = TRUE;
    $this->sbp = $this->config->get("{$plugin_id}_sbp") ?? FALSE;
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_secure"] = [
      '#type' => 'textfield',
      '#title' => "Секретное слово (для уведомлений)",
      '#default_value' => $config->get("{$this->plugin_id}_secure"),
    ];
    $form["{$this->plugin_id}_sbp"] = [
      '#type' => 'textfield',
      '#title' => "Обработчик СБП",
      '#description' => "По умолчанию sbp_default. Пример Точка Банк: sbp_tochka.",
      '#default_value' => $config->get("{$this->plugin_id}_sbp") ?? "sbp_default",
    ];
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    return $form;
  }
}
