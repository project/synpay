<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_payment\Entity\PaymentInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides PayKeeper Gateway.
 *
 * @SynpayAnnotation(
 *   id = "paykeeper",
 *   title = @Translation("Pay Keeper"),
 * )
 */
class SynpayPayKeeper extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Pay Keeper эквайринг"';
  const HOST = "https://synapse-studio.server.paykeeper.ru";
  const HOST_TEST = "https://demo.paykeeper.ru";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    $this->QR = FALSE;
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    $this->api = '';
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          if (!$this->mode) {
            $this->mode = 'test';
          }
          switch ($this->mode) {
            case 'test':
              $this->api = self::HOST_TEST;
              break;

            case 'live':
              $this->api = self::HOST;
              break;
          }
          $this->secure = $this->config->get("{$plugin_id}_secure");
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = \Drupal::httpClient([
      'timeout'  => 10.0,
    ]);
    $this->options = [
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
        'Cache-Control' => 'no-cache',
      ],
      'auth' => [trim($this->user ?? ''), trim($this->pass ?? '')],
    ];
    $url = "{$this->api}/info/settings/token/";
    $this->token = '';
    try {
      $response = $this->client->get($url, $this->options);
      $code = $response->getStatusCode();
      if ($code == 200) {
        $answer = $response->getBody()->getContents();
        $this->token = Json::decode($answer)['token'];
      }
    }
    catch (ClientException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, $total) {
    $order = Uuid::uuid4()->getHex()->toString();
    $data = $this->options;
    $data['form_params']['pay_amount'] = str_replace(',', '.', $order_amount);
    $data['form_params']['orderid'] = $order;
    $data['form_params']['token'] = $this->token;
    $url = "{$this->api}/info/options/byid/";
    try {
      $response = $this->client->post($url, $data);
      $code = $response->getStatusCode();
      $answer = $response->getBody()->getContents();
      $result = Json::decode($answer);
    }
    catch (ClientException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
    catch (RequestException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $amount = $payment->getAmount()->getNumber();
    $order_amount = (float) self::PRECISION ? $amount : bcmul($amount, 100, 0);
    if (is_null($this->client)) {
      return FALSE;
    }
    $order = $payment->getOrder();
    $profile = $order->getBillingProfile();
    $data = $this->options;
    $data['form_params']['orderid'] = $order_id;
    $data['form_params']['pay_amount'] = $order_amount;
    $data['form_params']['client_name'] = $profile->field_customer_name->value ?? '';
    $data['form_params']['client_email'] = $profile->field_customer_email->value ?? '';
    $data['form_params']['client_phone'] = $profile->field_customer_phone->value ?? '';
    $data['form_params']['token'] = $this->token;
    $url = "{$this->api}/change/invoice/preview/";
    try {
      $answer = $this->client->post($url, $data);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        if (isset($response['invoice_id'])) {
          $invoice_url = $response['invoice_url'];
          if ($this->QR) {
            $invoice_url .= "?pstype={$this->sbp}";
          }
          return [
            'orderId' => $response['invoice_id'],
            'formUrl' => $invoice_url,
          ];
        }
        else {
          \Drupal::messenger()->addError($response['Message']);
          \Drupal::messenger()->addError($response['Details']);
          return FALSE;
        }
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
    }
    return FALSE;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $paymentInfo = \Drupal::request()->request->all();

    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['payment_id' => $paymentInfo['orderid']]);
    $payment = reset($payments);
    if (!$payment) {
      return new Response('Bad request', 400);
    }
    $order = $payment->getOrder();
    if (!$order) {
      return new Response('Order not found', 404);
    }
    $key = md5($paymentInfo['id'] . number_format($paymentInfo['sum'], 2, ".", "") . $paymentInfo['orderid'] . $this->secure);
    if ($key == $paymentInfo['key']) {
      $payment->state = 'completed';
      $payment->setCompletedTime(time());
      $payment->setRemoteState('paid');
      $payment->save();
      if ($order->state->value != 'completed') {
        $event = new OrderEvent($order);
        $eventDispatcher = \Drupal::service('event_dispatcher');
        $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
        $order->getState()->applyTransitionById('place');
        $order->save();
      }
    }

    $answer = md5($paymentInfo['id'] . $this->secure);
    return new Response("OK $answer", 200);
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $result = 'completed';
    return $result;
  }

  /**
   * Check Payments.
   */
  private function checkPayment($id) {
    $params = $this->options;
    $params['PaymentId'] = $id;
    $data = [
      'Password' => $this->pass,
      'PaymentId' => $id,
      'TerminalKey' => $this->user,
    ];
    $token = '';
    foreach ($data as $key => $value) {
      $token .= $value;
    }
    $hash = hash('sha256', $token);
    $params['Token'] = $hash;
    try {
      $response = $this->client->post("{$this->api}GetState", ['json' => $params]);
      $code = $response->getStatusCode();
      $answer = Json::decode($response->getBody()->getContents());
      return $answer;
    }
    catch (ClientException $exception) {
      // $message = $exception->getMessage();
      // \Drupal::messenger()->addError($message);
    }
    return FALSE;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    else {
      switch ($status['Status']) {
        case 'CONFIRMED':
          $payment->state = 'completed';
          $payment->setCompletedTime(time());
          $payment->setRemoteState($status['Status']);
          $result = 'completed';
          break;

        default:
          $payment->state = 'authorization_voided';
          $payment->setRemoteState($status['Status']);
          $result = 'authorization_voided';
          break;
      }
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_secure"] = [
      '#type' => 'textfield',
      '#title' => "Секретное слово (для уведомлений)",
      '#default_value' => $config->get("{$this->plugin_id}_secure"),
    ];
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayPayKeeper')->info($message);
  }

}
