<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginInterface;

/**
 * Provides Yandex Gateway.
 *
 * @SynpayAnnotation(
 *   id = "yandex_split",
 *   title = @Translation("Yandex Split"),
 * )
 */
class SynpayYandexSplit extends SynpayYandex implements SynpayPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->availablePaymentMethods = ['SPLIT'];
  }

}
