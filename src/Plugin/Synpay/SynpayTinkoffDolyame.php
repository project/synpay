<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Component\Serialization\Json;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use GuzzleHttp\Exception\ClientException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides Tinkoff dolyame Gateway.
 *
 * @SynpayAnnotation(
 *   id = "tinkoff_dolyame",
 *   title = @Translation("Tinkoff Dolyame"),
 * )
 */
class SynpayTinkoffDolyame extends SynpayPluginBase implements SynpayPluginInterface {
  const URL = "https://partner.dolyame.ru/v1/";
  const CERT = '/var/www/html/sites/default/private/certificates/SynpayTinkoffDolyame/open-api-cert.pem';
  const SSL_KEY = [
    '/var/www/html/sites/default/private/certificates/SynpayTinkoffDolyame/private.key',
    'Hf34Fhjke8qhj44@sfk',
  ];

  // @todo remove. TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getCreateUrl() : string {
    return self::URL . 'orders/create';
  }

  /**
   * {@inheritdoc}
   */
  private function getCommitUrl(PaymentInterface $payment) : string {
    return sprintf('%sorders/%s/commit', self::URL, $payment->getRemoteId());
  }

  /**
   * {@inheritdoc}
   */
  private function getCancelUrl(PaymentInterface $payment) : string {
    return sprintf('%sorders/%s/cancel', self::URL, $payment->getRemoteId());
  }

  /**
   * {@inheritdoc}
   */
  private function getRefundUrl(PaymentInterface $payment) : string {
    return sprintf('%sorders/%s/refund', self::URL, $payment->getRemoteId());
  }

  /**
   * {@inheritdoc}
   */
  private function getInfoUrl(PaymentInterface $payment) : string {
    return sprintf('%sorders/%s/info', self::URL, $payment->getRemoteId());
  }

  /**
   * {@inheritdoc}
   */
  private function getOrderItemsData(OrderInterface $order) : array {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = self::PRECISION ? $price : bcmul($price, 100, 0);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = self::PRECISION ? $total : bcmul($total, 100, 0);
      $items[$c] = [
        'name' => $item->getTitle(),
        'price' => $price,
        'quantity' => $item->getQuantity(),
      ];
      $c++;
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $amount = $adjustment->getAmount()->getNumber();
      $amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
      $items[$c] = [
        'name' => $adjustment->getLabel(),
        'price' => round($amount, 2),
        'quantity' => 1,
      ];
      $c++;
    }
    // Foreach ($order->order_items as $item) {
    //   $order_item = $item->entity;
    //   $items[] = [
    //     'name' => $order_item->title->value,
    //     'quantity' => (int) $order_item->quantity->value,
    //     'price' => floatval(
    //       $order_item->getUnitPrice()
    //         ->getNumber()
    //     ),
    //   ];
    // }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  private function requestCreate(
    PaymentInterface $payment,
    string $fail_url,
    string $success_url
  ) : array {
    $order = $payment->getOrder();

    $parameters = [
      'order' => [
        'id' => $payment->getRemoteId(),
        'amount' => floatval(
          $order->getBalance()
            ->getNumber()
        ),
        'items' => $this->getOrderItemsData($order),
      ],
      'notification_url' => $this->getNotificationUrl($payment),
      'fail_url' => $fail_url,
      'success_url' => $success_url,
    ];

    $options = [
      'auth' => [$this->user, $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Correlation-ID' => Uuid::uuid4()->toString(),
      ],
      'body' => Json::encode($parameters),
      'cert' => self::CERT,
      'ssl_key' => self::SSL_KEY,
    ];
    try {
      $response = \Drupal::httpClient()
        ->post($this->getCreateUrl(), $options);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function requestInfo(PaymentInterface $payment) : array {
    $options = [
      'auth' => [$this->user, $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Correlation-ID' => Uuid::uuid4()->toString(),
      ],
      'cert' => self::CERT,
      'ssl_key' => self::SSL_KEY,
    ];
    try {
      $response = \Drupal::httpClient()
        ->get($this->getInfoUrl($payment), $options);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function requestCommit(PaymentInterface $payment) :? array {
    $order = $payment->getOrder();

    $parameters = [
      'amount' => floatval(
        $order->getBalance()
          ->getNumber()
      ),
      'items' => $this->getOrderItemsData($order),
    ];

    $options = [
      'auth' => [$this->user, $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Correlation-ID' => Uuid::uuid4()->toString(),
      ],
      'body' => Json::encode($parameters),
      'cert' => self::CERT,
      'ssl_key' => self::SSL_KEY,
    ];

    try {
      $response = \Drupal::httpClient()
        ->post($this->getCommitUrl($payment), $options);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function requestCancel(PaymentInterface $payment) :? array {
    $order = $payment->getOrder();

    $options = [
      'auth' => [$this->user, $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Correlation-ID' => Uuid::uuid4()->toString(),
      ],
      'cert' => self::CERT,
      'ssl_key' => self::SSL_KEY,
    ];

    try {
      $response = \Drupal::httpClient()
        ->post($this->getCancelUrl($payment), $options);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    $parameters = [
      'amount' => $amount,
      'returned_items' => $items,
    ];

    $options = [
      'auth' => [$this->user, $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
        'X-Correlation-ID' => Uuid::uuid4()->toString(),
      ],
      'body' => Json::encode($parameters),
      'cert' => self::CERT,
      'ssl_key' => self::SSL_KEY,
    ];

    try {
      $response = \Drupal::httpClient()
        ->post($this->getRefundUrl($payment), $options);
    }
    catch (ClientException $e) {
      $message = Json::decode($e->getResponse()->getBody()->getContents());
      $text = '';
      foreach ($message['operationDetails'] as $value) {
        $text .= "$value<br>";
      }
      return [
        'error' => $e->getResponse()->getStatusCode(),
        'text' => $text,
      ];
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function checkClientException(ClientException $e) {
    $response = $e->getResponse();
    $response_status_code = $response->getStatusCode();
    $message = Json::decode($response->getBody()->getContents());
    if ($response_status_code == 400) {
      throw new PaymentGatewayException('В случае некорректного формата запроса');
    }
    elseif ($response_status_code == 401) {
      throw new PaymentGatewayException(
        'Аутентификация не пройдена: введены неверные логин и/или пароль'
      );
    }
    elseif ($response_status_code == 403) {
      throw new PaymentGatewayException(
        'Авторизация не пройдена: пытаетесь работать с чужой заявкой'
      );
    }
    elseif ($response_status_code == 422) {
      throw new PaymentGatewayException(
        'Ошибка бизнес-логики: в текущем состоянии заявки нельзя выполнить это действие'
      );
    }
    elseif ($response_status_code == 500) {
      throw new PaymentGatewayException(
        'Неизвестная ошибка'
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getNotificationUrl(PaymentInterface $payment) : string {
    return sprintf(
      "%s/payment/notify/%s",
      \Drupal::request()->getSchemeAndHttpHost(),
      $payment->getPaymentGatewayId()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder(string $order_id, PaymentInterface $payment, array $params) {
    $order_id = str_replace('/', '_', $order_id);
    $payment->setRemoteId($order_id)->save();
    $answer = $this->requestCreate(
      $payment,
      $params['failUrl'],
      $params['returnUrl']
    );
    return [
      'orderId' => $order_id,
      'formUrl' => $answer['link'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function acquireLockName(OrderInterface $order) : string {
    return sprintf('payment-lock-name-%d', $order->id());
  }

  /**
   * Process.
   */
  private function completPayment(
    PaymentInterface $payment,
    string $remote_status
  ) {
    if (!in_array($remote_status, ['wait_for_commit', 'committed'])) {
      return;
    }
    if ($payment->state == 'completed') {
      return;
    }
    $payment->state = 'completed';
    $payment->setCompletedTime(time());

    $order = $payment->getOrder();
    $event = new OrderEvent($order);
    $eventDispatcher = \Drupal::service('event_dispatcher');
    $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
    if ($order->getState()->isTransitionAllowed('place')) {
      $order->getState()->applyTransitionById('place');
      $order->save();
    }
  }

  /**
   * Process.
   */
  private function commitOrCancelPayment(PaymentInterface $payment, string $remote_status) {
    $this->requestCommit($payment);
  }

  /**
   * Process.
   */
  private function rejectPayment(PaymentInterface $payment, string $remote_status) {
    if ($remote_status != 'rejected') {
      return;
    }
    elseif ($payment->state == 'authorization_voided') {
      return;
    }
    $payment->state = 'authorization_voided';
  }

  /**
   * Process.
   */
  private function processRequest(PaymentInterface $payment, string $notification_status) {
    $payment_info = $this->requestInfo($payment);
    $remote_status = $payment_info['status'] ?? '';
    switch ($notification_status) {
      case 'approved':
        break;

      case 'wait_for_commit':
        $this->commitOrCancelPayment($payment, $remote_status);
        break;

      case 'completed':
        $this->completPayment($payment, $remote_status);
        break;

      case 'canceled':
      case 'rejected':
        $this->rejectPayment($payment, $remote_status);
        break;
    }
    $payment->setRemoteState($remote_status);
    $payment->save();
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    return 'completed';
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $payment_info = Json::decode(
      file_get_contents('php://input')
    );
    if (empty($payment_info['id'])) {
      throw new PaymentGatewayException('Notify without payment Id');
    }
    $payment = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment')
      ->loadByRemoteId($payment_info['id']);
    $order = $payment->getOrder();
    $lock_name = $this->acquireLockName($order);
    $lock = \Drupal::lock();
    if ($lock->acquire($lock_name)) {
      $this->processRequest($payment, $payment_info['status']);
    }
    else {
      $lock->wait($lock_name);
      if ($lock->acquire($lock_name)) {
        $this->processRequest($payment, $payment_info['status']);
      }
      else {
        throw new \Exception("Couldn't acquire lock.");
      }
    }
    return new Response('OK', 200);
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_snippet"] = [
      '#type' => 'radios',
      '#title' => "Сниппет $this->title",
      '#options' => [
        'default' => "Выкл",
        'original' => "Оригинальный сниппет",
        'custom' => "Сниппет от Synapse",
      ],
      '#default_value' => $config->get("{$this->plugin_id}_snippet") ?? 'default',
    ];
    $form["{$this->plugin_id}_site_id"] = [
      '#type' => 'textfield',
      '#title' => "Site ID для виджета $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_site_id"),
    ];
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    // dsm($form);
    return $form;
  }

}
