<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Drupal\Core\Url;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Event\OrderEvent;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides CloudPayments Gateway.
 *
 * @SynpayAnnotation(
 *   id = "cloudpayments",
 *   title = @Translation("CloudPayments"),
 * )
 */
class SynpayCloudPayments extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"CloudPayments эквайринг"';
  const WIDGET = 'https://widget.cloudpayments.ru/bundles/cloudpayments.js';
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $plugin_id = $payment->payment_gateway->entity->getPluginConfiguration()['gateway'];
    $url = "{$this->host}/synpay/onsite/$plugin_id/{$payment->id()}";
    return [
      'orderId' => $order_id,
      'formUrl' => $url,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function onsite($paymentId) {
    $payment = \Drupal::EntityTypeManager()->getStorage('commerce_payment')->load($paymentId);
    if (!$payment) {
      return [
        'error' => TRUE,
        'url' => '',
      ];
    }
    $order = $payment->getOrder();
    if (!empty($payment->getRemoteState())) {
      $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $order->id(), 'step' => 'review']);
      return [
        'error' => TRUE,
        'url' => $url,
      ];
    }
    $amount = $payment->getAmount()->getNumber();
    $currency = $payment->getAmount()->getCurrencyCode();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    $data = [
      'pay' => 'charge',
      'publicId' => $this->config->get("{$this->plugin_id}_publicId"),
      'amount' => round($order_amount, 2),
      'currency' => $currency,
      'accountId' => $order->getEmail(),
      'requireEmail' => FALSE,
      'description' => "Счет № {$order->id()}",
      'invoiceId' => $payment->getRemoteId(),
      'skin' => $this->config->get("{$this->plugin_id}_skin"),
    ];
    return [
      'text' => 'Оплата через CloudPayments',
      'data' => $data,
      'library' => [
        'synpay/synpay',
        'synpay/cloudpayments',
      ],
    ];
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  private function getItems(OrderInterface $order) {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = self::PRECISION ? $price : bcmul($price, 100, 0);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = self::PRECISION ? $total : bcmul($total, 100, 0);
      $items[$c] = [
        'Name' => $item->getTitle(),
        'Price' => $price,
        'Quantity' => $item->getQuantity(),
        'Amount' => $total,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      $c++;
    }
    foreach ($order->getAdjustments() as $adjustment) {
      $amount = $adjustment->getAmount()->getNumber();
      $amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
      $items[$c] = [
        'Name' => $adjustment->getLabel(),
        'Price' => $amount,
        'Quantity' => 1,
        'Amount' => $amount,
        'Tax' => $this->config->get("{$this->plugin_id}_tax"),
      ];
      switch ($adjustment->getType()) {
        case 'shipping':
          $items[$c]['PaymentObject'] = 'service';
          break;
      }
      $c++;
    }
    return $items;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    return new Response('OK', 200);
  }

  /**
   * Commerce Return.
   */
  public function callback(Request $request) {
    $paymentInfo = $request->request->all() ?? '';
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
      '@j', ['@j' => json_encode($paymentInfo ?? [])]
    );
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $paymentInfo['InvoiceId']]);
    $payment = reset($payments);
    if (!$payment) {
      $payments = $payment_storage->loadByProperties(['remote_id' => $paymentInfo['TransactionId']]);
      $payment = reset($payments);
    }
    if ($payment) {
      $payment->setRemoteId($paymentInfo['TransactionId']);
      $order = $payment->getOrder();
      switch ($paymentInfo['Status']) {
        case 'Completed':
          $payment->setState('completed');
          $payment->setCompletedTime(strtotime($paymentInfo['DateTime']));
          if ($order->getState()->getString() == 'draft') {
            $event = new OrderEvent($order);
            $eventDispatcher = \Drupal::service('event_dispatcher');
            $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
            $order->getState()->applyTransitionById('place');
            $order->save();
          }
          break;

        case 'Declined':
          $payment->setState('authorization_voided');
          break;
      }
      $payment->setRemoteState($paymentInfo['Status']);
      $payment->save();
    }
    return new JsonResponse(['code' => 0]);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
      $this->updatePayment($remote_id, $answer);
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Check Payments.
   */
  private function checkPayment($id) {
    return FALSE;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    else {
      switch ($status['Status']) {
        case 'CONFIRMED':
        case 'AUTHORIZED':
          $payment->state = 'completed';
          $payment->setCompletedTime(time());
          $payment->setRemoteState($status['Status']);
          $result = 'completed';
          break;

        default:
          $payment->state = 'authorization_voided';
          $payment->setRemoteState($status['Status']);
          $result = 'authorization_voided';
          break;
      }
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return '';
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    // $form["{$this->plugin_id}_test_login"] = [
    //   '#type' => 'textfield',
    //   '#title' => "Тестовый логин $this->title",
    //   '#maxlength' => 100,
    //   '#size' => 100,
    //   '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    // ];
    // $form["{$this->plugin_id}_test_token"] = [
    //   '#type' => 'textfield',
    //   '#title' => "Тестовый токен $this->title",
    //   '#maxlength' => 100,
    //   '#size' => 100,
    //   '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    // ];
    // $form["{$this->plugin_id}_live_login"] = [
    //   '#type' => 'textfield',
    //   '#title' => "Рабочий логин $this->title",
    //   '#maxlength' => 100,
    //   '#size' => 100,
    //   '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    // ];
    // $form["{$this->plugin_id}_live_token"] = [
    //   '#type' => 'textfield',
    //   '#title' => "Рабочий токен $this->title",
    //   '#maxlength' => 100,
    //   '#size' => 100,
    //   '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    // ];
    $form["{$this->plugin_id}_publicId"] = [
      '#type' => 'textfield',
      '#title' => 'ID из личного кабинета',
      '#default_value' => $config->get("{$this->plugin_id}_publicId"),
    ];
    $form["{$this->plugin_id}_skin"] = [
      '#type' => 'select',
      '#title' => 'Дизайн виджета',
      '#options' => [
        'classic' => 'classic',
        'modern' => 'modern',
        'mini' => 'mini',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_skin"),
    ];
    return $form;
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayTinkoff')->info($message);
  }

}
