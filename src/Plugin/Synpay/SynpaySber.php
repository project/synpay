<?php

namespace Drupal\synpay\Plugin\Synpay;

use Ramsey\Uuid\Uuid;
use GuzzleHttp\Client;
use Voronkovich\SberbankAcquiring\Currency;
use Symfony\Component\HttpFoundation\Request;
use Voronkovich\SberbankAcquiring\OrderStatus;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Voronkovich\SberbankAcquiring\Client as SberbankClient;
use Voronkovich\SberbankAcquiring\Exception\ActionException;
use Voronkovich\SberbankAcquiring\HttpClient\GuzzleAdapter as SberbankGuzzleAdapter;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a 'Sber' PayPlugin.
 *
 * Visa:
 * Номер карты 4111 1111 1111 1111
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=y, pares=y
 * Проверочный код 3-D Secure 12345678
 *
 * MasterCard:
 * Номер карты 5555 5555 5555 5599
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=n
 *
 * Мир:
 * Номер карты 2200 0000 0000 0053
 * Дата истечения срока действия 2024/12
 * Проверочный код на обратной стороне 123
 * 3-D Secure veres=n
 * Проверочный код 3-D Secure 12345678
 *
 * @SynpayAnnotation(
 *   id = "sber",
 *   title = @Translation("Sber"),
 * )
 */
class SynpaySber extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Сбербанк эквайринг"';
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $path = \Drupal::service('extension.list.module')->getPath('synpay');
    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->client = NULL;
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = Currency::RUB;
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    $this->taxation = $this->config->get("{$plugin_id}_taxation");
    $this->payment_object = $this->config->get("{$plugin_id}_payment_object");
    $this->payment_method = $this->config->get("{$plugin_id}_payment_method");

    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $api = SberbankClient::API_URI_TEST;
              break;

            case 'live':
              $api = SberbankClient::API_URI;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = new SberbankClient([
      'userName' => trim($this->user ?? ''),
      'password' => trim($this->pass ?? ''),
      'language' => $this->lang,
      // ISO 4217 currency code.
      'currency' => $this->currency,
      'apiUri' => $api ?? '',
      'httpClient' => new SberbankGuzzleAdapter(new Client(['verify' => "{$path}/assets/certs/russian_trusted_root_ca.pem"])),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $return = "$this->host/synpay/return_direct/$this->plugin_id";
    $params = [
      'failUrl' => "$this->host/synpay/return_direct/$this->plugin_id?fail",
    ];
    $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = bcmul($total, 100, 0);
    try {
      $result = $this->client->registerOrder($order, $order_amount, $return, $params);
      $redirect = $result['formUrl'];
      $response = new TrustedRedirectResponse($redirect);
      $response->send();
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    // Execute request to Sberbank.
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    if (!is_null($this->taxation)) {
      $params['taxSystem'] = $this->taxation;
    }
    if (is_null($this->client)) {
      return FALSE;
    }
    if (isset($params['orderBundle']['cartItems']['items'])) {
      $items = $params['orderBundle']['cartItems']['items'];
      foreach ($items as $key => $item) {
        $params['orderBundle']['cartItems']['items'][$key]['itemAttributes'] = [
          'paymentMethod' => $this->payment_method,
          'paymentObject' => $this->payment_object,
        ];
      }
    }
    try {
      $result = $this->client->registerOrder($order_id, $order_amount, $params['returnUrl'], $params);
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
      \Drupal::logger('synpay_sber')->error($message);
      return FALSE;
    }
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $lock = \Drupal::lock();
    $payment_lock_name = $this->acquirePaymentLockName($order->id());
    if (!$lock->acquire($payment_lock_name, 8.0)) {
      $lock->wait($payment_lock_name, 8.0);
      $lock->acquire($payment_lock_name, 8.0);
    }
    $remote_id = $request->query->get('orderId') ?? '';
    $answer = $this->client->getOrderStatus($remote_id);
    $result = $this->updatePayment($remote_id, $answer);
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $remote_id = $request->query->get('mdOrder') ?? '';
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $remote_id]);
    $payment = reset($payments);
    $lock = \Drupal::lock();
    $payment_lock_name = $this->acquirePaymentLockName($payment->getOrderId());
    if (!$lock->acquire($payment_lock_name, 8.0)) {
      $lock->wait($payment_lock_name, 8.0);
      $lock->acquire($payment_lock_name, 8.0);
    }
    $answer = $this->client->getOrderStatus($remote_id);
    $this->updatePayment($remote_id, $answer);
    return new Response('OK', 200);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    try {
      $answer = $this->client->getOrderStatus($remote_id);
      // if (\Drupal::currentUser()->id() == 1) {
      //   dsm($answer);
      // }
      $answer['status'] = $answer['paymentAmountInfo']['paymentState'];
      if ($answer['actionCode'] != 0) {
        $answer['status'] .= "<br> {$answer['actionCodeDescription']}";
      }
    }
    catch (ActionException $exception) {
      $answer['status'] = $exception->getMessage();
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    try {
      $answer = $this->client->getOrderStatus($remote_id);
      $answer['status'] = $answer['paymentAmountInfo']['paymentState'];
      if ($answer['actionCode'] != 0) {
        $answer['status'] .= "<br> {$answer['actionCodeDescription']}";
      }
      $this->updatePayment($remote_id, $answer);
    }
    catch (ActionException $exception) {
      $answer['status'] = $exception->getMessage();
    }
    return $answer;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($remote_id, $answer) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($remote_id);
    $order = $payment->getOrder();
    switch ($answer['orderStatus']) {
      case OrderStatus::DEPOSITED:
        $result = 'completed';
        $payment->setState($result);
        $payment->setCompletedTime(time());
        break;

      default:
      case OrderStatus::DECLINED:
        $result = 'authorization_voided';
        $payment->setState($result);
    }
    $payment->setRemoteState($answer['paymentAmountInfo']['paymentState']);
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  private function acquirePaymentLockName(string $orderId) : string {
    return sprintf('payment-lock-name-%d', $orderId);
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "Карта для использования:<br>
            Номер: 4111 1111 1111 1111, Срок действия: 12/24, CVC2/CVV2 код: 123, Пароль: 12345678";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый пароль $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий пароль $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    $form["{$this->plugin_id}_send_receipt"] = [
      '#type' => 'select',
      '#title' => 'Передавать данные для формирования чека',
      '#options' => [
          'no' => 'Нет',
          'yes' => 'Да'
      ],
      '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    ];
    $form["{$this->plugin_id}_taxation"] = [
      '#type' => 'select',
      '#title' => 'Система налогообложения',
      '#options' => [
        '0' => 'общая',
        '1' => 'упрощённая, доход',
        '2' => 'упрощённая, доход минус расход',
        '3' => 'единый налог на вменённый доход',
        '4' => 'единый сельскохозяйственный налог',
        '5' => 'патентная система налогообложения',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_taxation") ?? 0,
    ];
    $form["{$this->plugin_id}_payment_method"] = [
      '#type' => 'select',
      '#title' => 'Признак способа расчёта',
      '#options' => [
        '1' => 'полная предварительная оплата до момента передачи предмета расчёта',
        '2' => 'частичная предварительная оплата до момента передачи предмета расчёта',
        '3' => 'аванс',
        '4' => 'полная оплата в момент передачи предмета расчёта',
        '5' => 'частичная оплата предмета расчёта в момент его передачи с последующей оплатой в кредит',
        '6' => 'передача предмета расчёта без его оплаты в момент его передачи с последующей оплатой в кредит',
        '7' => 'оплата предмета расчёта после его передачи с оплатой в кредит',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_method"),
    ];
    $form["{$this->plugin_id}_payment_object"] = [
      '#type' => 'select',
      '#title' => 'Признак предмета расчёта',
      '#options' => [
        '1' => 'товар',
        '2' => 'подакцизный товар',
        '3' => 'работа',
        '4' => 'услуга',
        '5' => 'ставка азартной игры',
        '6' => 'выигрыш азартной игры',
        '7' => 'лотерейный билет',
        '8' => 'выигрыш лотереи',
        '9' => 'предоставление РИД',
        '10' => 'платёж',
        '11' => 'агентское вознаграждение',
        '12' => 'составной предмет расчёта',
        '13' => 'иной предмет расчёта',
        '14' => 'имущественное право',
        '15' => 'внереализационный доход',
        '16' => 'страховые взносы',
        '17' => 'торговый сбор',
        '18' => 'курортный сбор',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_object"),
    ];
    return $form;
  }

}
