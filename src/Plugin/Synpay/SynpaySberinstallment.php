<?php

namespace Drupal\synpay\Plugin\Synpay;

/**
 * Provides a 'Sberinstallment' PayPlugin.
 *
 * @SynpayAnnotation(
 *   id = "sberinstallment",
 *   title = @Translation("Sberbank installment"),
 * )
 */
class SynpaySberinstallment extends SynpaySbercredit {

}
