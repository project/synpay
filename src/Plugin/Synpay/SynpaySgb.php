<?php

namespace Drupal\synpay\Plugin\Synpay;

use Ramsey\Uuid\Uuid;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides a 'Sgb' PayPlugin.
 *
 * @SynpayAnnotation(
 *   id = "sgb",
 *   title = @Translation("Severgasbank"),
 * )
 */
class SynpaySgb extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Севергазбанк эквайринг"';
  const HOST = "https://mpi.severgazbank.ru/payment/";
  const HOST_TEST = "https://web.rbsuat.com/sgb/";
  const REGISTER = "rest/register.do";
  const DEPOSIT = "rest/deposit.do";
  const STATUS = "rest/getOrderStatusExtended.do";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $this->api = self::HOST_TEST;
              break;

            case 'live':
              $this->api = self::HOST;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = \Drupal::httpClient();
    $this->options = [
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
        'Cache-Control' => 'no-cache',
      ],
      'form_params' => [
        'userName' => trim($this->user ?? ''),
        'password' => trim($this->pass ?? ''),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $params = [
      'returnUrl' => "$host/synpay/return_direct/$this->plugin_id",
      'failUrl' => "$host/synpay/return_direct/$this->plugin_id?fail",
    ];
    $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = $total * 100;
    $result = $this->registerOrder($order, $order_amount, $params);
    if ($result) {
      $response = new TrustedRedirectResponse($result['formUrl']);
      $response->send();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    if (is_null($this->client)) {
      return FALSE;
    }
    $url = $this->api . self::REGISTER;
    $data = $this->options;
    $data['form_params']['orderNumber'] = $order_id;
    $data['form_params']['amount'] = $order_amount;
    $data['form_params']['returnUrl'] = $params['returnUrl'];
    $data['form_params']['failUrl'] = $params['failUrl'];
    if (!empty($params['currency'])) {
      $data['form_params']['currency'] = $params['currency'];
    }
    if (!empty($params['orderBundle']['customerDetails']['email'])) {
      $data['form_params']['email'] = $params['orderBundle']['customerDetails']['email'];
    }
    try {
      $answer = $this->client->request('POST', $url, $data);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        return [
          'data' => $data,
          'orderId' => $response['orderId'],
          'formUrl' => $response['formUrl'],
        ];
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
    return FALSE;
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $remote_id = $request->get('mdOrder');
    // $result = $this->getOrderStatus($remote_id);
    if (!empty($remote_id)) {
      $status = $this->getOrderStatus($remote_id);
      $result = $this->updatePayment($remote_id, $status);
    }
    else {
      $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
      $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
      $success = FALSE;
      foreach ($payments as $payment) {
        $status = $this->getOrderStatus($payment->getRemoteId());
        $answer = $this->updatePayment($payment->getRemoteId(), $status);
        if ($answer == 'completed' && !$success) {
          $success = TRUE;
        }
      }
      $result = $success ? 'completed' : $answer;
    }
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onReturnTest(OrderInterface $order) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    $success = FALSE;
    foreach ($payments as $payment) {
      $status = $this->getOrderStatus($payment->getRemoteId());
      $answer = $this->updatePayment($payment->getRemoteId(), $status);
      if ($answer == 'completed' && !$success) {
        $success = TRUE;
      }
    }
    $result = $success ? 'completed' : $answer;
    return $result;
  }

  /**
   * Pay callback.
   */
  public function callback(Request $request) {
    // $remote_id = $request->get('mdOrder');
    // $result = $this->getOrderStatus($remote_id);
    // dsm($result);
    return $result;
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $remote_id = $request->query->get('mdOrder') ?? '';
    $answer = $this->getOrderStatus($remote_id);
    $this->updatePayment($remote_id, $answer);
    return new Response('OK', 200);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    // if (\Drupal::currentUser()->id() == 1) {
    //   dsm($answer);
    // }
    if ($answer['answer']['errorCode'] == 0) {
      if ($answer['answer']['actionCode'] != 0) {
        $answer['status'] .= "<br> {$answer['answer']['actionCodeDescription']}";
      }
    }
    return $answer;
  }

  /**
   * Commerce Return.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['answer']['errorCode'] == 0) {
      if ($answer['answer']['actionCode'] != 0) {
        $answer['status'] .= "<br> {$answer['answer']['actionCodeDescription']}";
      }
      $this->updatePayment($remote_id, $answer);
    }
    return $answer;
  }

  /**
   * Get order status from sgb.
   */
  private function getOrderStatus(string $remote_id) {
    $url = $this->api . self::STATUS;
    $data = $this->options;
    $data['form_params']['orderId'] = $remote_id;
    try {
      $answer = $this->client->request('POST', $url, $data);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        $result['answer'] = $response;
        if ($response['errorCode'] != 0) {
          $result['status'] = $response['errorMessage'];
          return $result;
        }
        switch ($response['orderStatus']) {
          case 0:
            $result['status'] = 'Заказ зарегистрирован, но не оплачен';
            break;

          case 2:
            $result['status'] = 'completed';
            break;

          case 3:
            $result['status'] = 'Авторизация отменена';
            break;

          case 6:
            $result['status'] = 'Авторизация отклонена';
            break;

          default:
            $result['status'] = 'Ошибка';
        }
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      $result['status'] = $message;
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
    return $result;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($remote_id, $answer) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($remote_id);
    $order = $payment->getOrder();
    switch ($answer['status']) {
      case 'completed':
        $result = 'completed';
        $payment->state = $result;
        $payment->setCompletedTime($answer['answer']['authDateTime'] / 1000);
        $payment->setRemoteState($answer['answer']['paymentAmountInfo']['paymentState']);
        $payment->save();
        if ($order->state->value == 'draft') {
          $event = new OrderEvent($order);
          $eventDispatcher = \Drupal::service('event_dispatcher');
          $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
          $order->getState()->applyTransitionById('place');
          $order->save();
        }
        break;

      default:
        $result = 'authorization_voided';
        $payment->state = $result;
        $payment->setRemoteState($answer['answer']['paymentAmountInfo']['paymentState']);
        $payment->save();
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    return $form;
  }

}
