<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\Component\Serialization\Json;
use Drupal\synpay\PluginManager\SynpayPluginInterface;

/**
 * Provides Tinkoff Gateway.
 *
 * @SynpayAnnotation(
 *   id = "tinkoff_qr",
 *   title = @Translation("Tinkoff Qr"),
 * )
 */
class SynpayTinkoffQr extends SynpayTinkoff implements SynpayPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Обработчик ответа на запрос регистрации заказа.
   */
  public function registerOrderResponse(array $response) : array {
    return $this->getQr($response);
  }

  /**
   * Получение Qr.
   */
  private function getQr(array $response) : array {
    $terminal_key = trim($this->user);
    $payment_id = $response['PaymentId'];
    $token = hash('sha256', $terminal_key . $payment_id);
    $data = [
      'TerminalKey' => $terminal_key,
      'PaymentId' => $payment_id,
      'Token' => $token,
    ];
    $answer = $this->client->post("{$this->api}GetQr", ['json' => $data]);
    $code = $answer->getStatusCode();
    if ($code == 200) {
      $body = $answer->getBody()->getContents();
      $response = JSON::decode($body);
      if ($response['Success']) {
        return [
          'orderId' => $response['PaymentId'],
          'formUrl' => $response['Data'],
        ];
      }
      else {
        \Drupal::messenger()->addError($response['Message']);
        \Drupal::messenger()->addError($response['Details']);
        return [];
      }
    }
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('SynpayTinkoff')->info($message);
  }

}
