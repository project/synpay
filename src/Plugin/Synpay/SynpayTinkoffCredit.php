<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Ramsey\Uuid\Uuid;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Event\OrderEvent;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides Tinkoff credit Gateway.
 *
 * @SynpayAnnotation(
 *   id = "tinkoff_credit",
 *   title = @Translation("Tinkoff Credit"),
 * )
 */
class SynpayTinkoffCredit extends SynpayPluginBase implements SynpayPluginInterface {
  const URL = "https://forma.tinkoff.ru/api/partners/v2/";
  // @todo remove TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getCreateUrl() : string {
    // $mode = $this->config->get(
    //   sprintf('%s_test_mode', $this->plugin_id)
    // );
    if ($this->mode == 'test') {
      return self::URL . 'orders/create-demo';
    }
    return self::URL . 'orders/create';
  }

  /**
   * {@inheritdoc}
   */
  private function getAuthUser() : string {
    // $mode = $this->config->get(
    //   sprintf('%s_test_mode', $this->plugin_id)
    // );
    if ($this->mode == 'test') {
      return "demo-" . $this->getShowcaseId();
    }
    return $this->getShowcaseId();
  }

  /**
   * {@inheritdoc}
   */
  private function getInfoUrl(PaymentInterface $payment) : string {
    return sprintf('%sorders/%d/info', self::URL, $payment->id());
  }

  /**
   * {@inheritdoc}
   */
  protected function getPromoCode(OrderInterface $order) :? string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getShowcaseId() :? string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getShopId() :? string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAutoVerifyState() :? bool {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function requestCreate(
    PaymentInterface $payment,
    string $order_id,
    string $fail_url,
    string $success_url
  ) : array {
    $order = $payment->getOrder();
    if (!$this->getShopId()) {
      throw new PaymentGatewayException('Не указан "ShopId"');
    }
    elseif (!$this->getShowcaseId()) {
      throw new PaymentGatewayException('Не указан "ShowcaseId"');
    }
    $parameters = [
      'shopId' => $this->getShopId(),
      'showcaseId' => $this->getShowcaseId(),
      'sum' => $order->getBalance()->getNumber(),
      'items' => $this->getOrderItems($order),
      'orderNumber' => $order_id,
      'promoCode' => $this->getPromoCode($order),
      'failURL' => $fail_url,
      'successURL' => $success_url,
      'returnURL' => $success_url,
      'webhookURL' => $this->getNotificationUrl($payment),
    ];
    $headers = [
      'Content-Type' => 'application/json',
    ];

    try {
      $response = \Drupal::httpClient()
        ->post($this->getCreateUrl(), [
          'body' => Json::encode($parameters),
          'headers' => $headers,
        ]);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function requestInfo(PaymentInterface $payment) : array {
    $options = [
      'auth' => [$this->getAuthUser(), $this->pass],
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ];
    try {
      $response = \Drupal::httpClient()
        ->get($this->getInfoUrl($payment), $options);
    }
    catch (ClientException $e) {
      $this->checkClientException($e);
    }

    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 200) {
      return Json::decode(
        $response->getBody()
          ->getContents()
      );
    }
    throw new PaymentGatewayException(
      'Bad feedback response, missing feedback parameter.'
    );
  }

  /**
   * {@inheritdoc}
   */
  private function requestCommit(PaymentInterface $payment) :? array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function requestCancel(PaymentInterface $payment) :? array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function checkClientException(ClientException $e) {
    $response = $e->getResponse();
    $response_status_code = $response->getStatusCode();
    if ($response_status_code == 400) {
      throw new PaymentGatewayException('В случае некорректного формата запроса');
    }
    elseif ($response_status_code == 401) {
      throw new PaymentGatewayException(
        'Аутентификация не пройдена: введены неверные логин и/или пароль'
      );
    }
    elseif ($response_status_code == 403) {
      throw new PaymentGatewayException(
        'Авторизация не пройдена: пытаетесь работать с чужой заявкой'
      );
    }
    elseif ($response_status_code == 422) {
      throw new PaymentGatewayException(
        'Ошибка бизнес-логики: в текущем состоянии заявки нельзя выполнить это действие'
      );
    }
    elseif ($response_status_code == 500) {
      throw new PaymentGatewayException(
        'Неизвестная ошибка'
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getOrderItems(OrderInterface $order) : array {
    $items = [];
    foreach ($order->order_items as $item) {
      $order_item = $item->entity;
      $quantity = $order_item->quantity->value;
      $price = $order_item->unit_price->number;
      $items[] = [
        'name' => $order_item->title->value,
        'quantity' => (int) $quantity,
        'price' => doubleval($price),
      ];
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  private function getNotificationUrl(PaymentInterface $payment) : string {
    return sprintf(
      "%s/payment/notify/%s",
      \Drupal::request()->getSchemeAndHttpHost(),
      $payment->getPaymentGatewayId()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $answer = $this->requestCreate(
      $payment,
      $order_id,
      $params['failUrl'],
      $params['returnUrl']
    );

    return [
      'orderId' => $answer['id'],
      'formUrl' => $answer['link'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function acquirePaymentLockName(PaymentInterface $payment) : string {
    return sprintf('payment-lock-name-%d', $payment->id());
  }

  /**
   * Process.
   */
  private function completPayment(
    PaymentInterface $payment,
    string $remote_status
  ) {
    if (!in_array($remote_status, ['signed', 'approved'])) {
      return;
    }
    if ($payment->state == 'completed') {
      return;
    }
    $payment->state = 'completed';
    $payment->setCompletedTime(time());

    $order = $payment->getOrder();
    $event = new OrderEvent($order);
    $eventDispatcher = \Drupal::service('event_dispatcher');
    $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
    if ($order->getState()->isTransitionAllowed('place')) {
      $order->getState()->applyTransitionById('place');
      $order->save();
    }
  }

  /**
   * Process.
   */
  private function commitOrCancelPayment(PaymentInterface $payment, string $remote_status) {
    // @todo если автоподтверждение ВЫКЛ, сделать commit или cancel
    if ($this->getAutoVerifyState()) {
      $this->requestCommit($payment);
    }
  }

  /**
   * Process.
   */
  private function rejectPayment(PaymentInterface $payment, string $remote_status) {
    if ($remote_status != 'rejected') {
      return;
    }
    elseif ($payment->state == 'authorization_voided') {
      return;
    }
    $payment->state = 'authorization_voided';
  }

  /**
   * Process.
   */
  private function processRequest(PaymentInterface $payment, string $notification_status) {
    $payment_info = $this->requestInfo($payment);
    $remote_status = $payment_info['status'] ?? '';
    switch ($notification_status) {
      case 'approved':
        $this->completPayment($payment, $remote_status);
        break;

      case 'signed':
        $this->completPayment($payment, $remote_status);
        $this->commitOrCancelPayment($payment, $remote_status);
        break;

      case 'rejected':
        $this->rejectPayment($payment, $remote_status);
        break;
    }
    $payment->setRemoteState($remote_status);
    $payment->save();
  }

  /**
   * Commerce Return.
   */
  public function onReturn(OrderInterface $order, Request $request) {
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    $payment_info = Json::decode(
      file_get_contents('php://input')
    );
    if (empty($payment_info['id'])) {
      throw new PaymentGatewayException('Notify without payment Id');
    }
    $payment = \Drupal::entityTypeManager()->getStorage('commerce_payment')
      ->load($payment_info['id']);

    $payment_lock_name = $this->acquirePaymentLockName($payment);
    $lock = \Drupal::lock();
    if ($lock->acquire($payment_lock_name)) {
      $this->processRequest($payment, $payment_info['status']);
    }
    else {
      $lock->wait($payment_lock_name);
      if ($lock->acquire($payment_lock_name)) {
        $this->processRequest($payment, $payment_info['status']);
      }
      else {
        throw new \Exception("Couldn't acquire lock.");
      }
    }
    return new Response('OK', 200);
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    // $form["{$this->plugin_id}_taxation"] = [
    //   '#type' => 'select',
    //   '#title' => 'Taxation system',
    //   '#options' => [
    //     'osn' => 'Общая',
    //     'usn_income' => 'Упрощенная (доходы)',
    //     'usn_income_outcome' => 'Упрощенная (доходы минус расходы)',
    //     'patent' => 'Патентная',
    //     'envd' => 'Единый налог на вмененный доход',
    //     'esn' => 'Единый сельскохозяйственный налог',
    //   ],
    //   '#default_value' => $config->get("{$this->plugin_id}_taxation"),
    // ];
    return $form;
  }

}
