<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Response;
use SimpleXMLElement;

/**
 * Provides Robokassa Gateway.
 *
 * @SynpayAnnotation(
 *   id = "robokassa",
 *   title = @Translation("Robokassa"),
 * )
 */
class SynpayRobokassa extends SynpayPluginBase implements SynpayPluginInterface {
  const ROBOURL = "https://auth.robokassa.ru/Merchant/Index.aspx";
  const ROBOCHECKURL = "https://auth.robokassa.ru/Merchant/WebService/Service.asmx/OpState?";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->taxation = $this->config->get("{$plugin_id}_taxation");
    $this->payment_object = $this->config->get("{$plugin_id}_payment_object");
    $this->payment_method = $this->config->get("{$plugin_id}_payment_method");
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway->getQuery()->accessCheck(TRUE)->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          $this->user = $this->config->get("{$plugin_id}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_pass");
          $this->pass2 = $this->config->get("{$plugin_id}_{$this->mode}_pass2");
          $this->desc = $this->config->get("{$plugin_id}_description");
        }
      }
    }
    $this->client = \Drupal::httpClient();
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $return = "$host/synpay/callback/{$this->plugin_id}";
    $params = [
      'failUrl' => "$host/synpay/callback/{$this->plugin_id}?fail",
    ];
    // $order = Uuid::uuid4()->getHex()->toString();
    $order_amount = str_replace(',', '.', $total);
    $crc = md5("{$this->user}:$order_amount:$order:{$this->pass}");
    try {
      $redirect = self::ROBOURL;
      $redirect .= "MerchantLogin={$this->user}&OutSum=$order_amount&InvoiceID=$order&SignatureValue=$crc&Description=" . $this->desc;
      if ($this->mode == 'test') {
        $redirect .= '&IsTest=1';
      }
      $response = new TrustedRedirectResponse($redirect);
      $response->send();
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    $payment_id = $payment->id();
    $amount = $payment->getAmount()->getNumber();
    $order_amount = self::PRECISION ? $amount : bcmul($amount, 100, 0);
    if (isset($params['orderBundle']['cartItems']['items'])) {
      $items = $params['orderBundle']['cartItems']['items'];
      foreach ($items as $key => $item) {
        $params['orderBundle']['cartItems']['items'][$key]['payment_method'] = $this->payment_method;
        $params['orderBundle']['cartItems']['items'][$key]['payment_object'] = $this->payment_object;
      }
    }
    $receipt = json_encode([
      'sno' => $this->taxation,
      'items' => $params['orderBundle']['cartItems']['items'],
    ]);
    if ($this->mode == 'test') {
      $crc = md5("{$this->user}:$order_amount:$payment_id:{$this->pass}");
      $data = [
        'MerchantLogin' => $this->user,
        'OutSum' => $order_amount,
        'InvoiceID' => "$payment_id",
        'Description' => $this->desc,
        'SignatureValue' => $crc,
        'IsTest' => 1,
      ];
    }
    else {
      $crc = md5("{$this->user}:$order_amount:$payment_id:$receipt:{$this->pass}");
      $data = [
        'MerchantLogin' => $this->user,
        'OutSum' => $order_amount,
        'InvoiceID' => "$payment_id",
        'Description' => $this->desc,
        'Receipt' => $receipt,
        'SignatureValue' => $crc,
      ];
    }
    // dsm($data);
    return [
      'data' => $data,
      'orderId' => $order_id,
      'url' => self::ROBOURL,
    ];
  }

  /**
   * CallBack.
   */
  public function return(Request $request) {
    $remote_id = $request->get('InvId');
    if (!$remote_id) {
      return [
        'status' => 'error',
      ];
    }
    $result = [
      'remote_id' => $remote_id,
    ];
    $sum = $request->get('OutSum');
    $crc = strtoupper($request->get('SignatureValue'));
    $my_crc = strtoupper(md5("$sum:$remote_id:{$this->pass}"));
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->load($remote_id);
    $result['order_id'] = $payment->getOrderId();
    if ($my_crc == $crc) {
      $result['status'] = 'completed';
    }
    else {
      $result['status'] = 'error';
    }
    return $result;
  }

  /**
   * Pay callback.
   */
  public function callback(Request $request) {
    $remote_id = $request->get('InvId');
    $sum = $request->get('OutSum');
    $crc = strtoupper($request->get('SignatureValue'));
    $my_crc = strtoupper(md5("$sum:$remote_id:{$this->pass2}"));
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->load($remote_id);
    $order = $payment->getOrder();
    // $result['order_id'] = $payment->getOrderId();
    if ($my_crc == $crc) {
      // $result['status'] = 'completed';
      $status = 'completed';
      $payment->state = $status;
      $payment->setCompletedTime(time());
      $event = new OrderEvent($order);
      $eventDispatcher = \Drupal::service('event_dispatcher');
      $eventDispatcher->dispatch($event, CheckoutEvents::COMPLETION);
      $order->getState()->applyTransitionById('place');
      $order->save();
    }
    else {
      // $result['status'] = 'authorization_voided';
      $status = 'authorization_voided';
      $payment->state = $status;
    }
    $payment->setRemoteState($status);
    $payment->save();
    return new Response("OK{$order->id()}", 200);
  }


  /**
   * Commerce Return.
   */
  // public function onReturn(string $remote_id) {
  //   $result = $this->client->getOrderStatus($remote_id);
  //   switch ($result['orderStatus']) {
  //     case OrderStatus::DEPOSITED:
  //       $result['status'] = 'completed';
  //       break;
  //
  //     default:
  //     case OrderStatus::DECLINED:
  //       $result['status'] = 'authorization_voided';
  //   }
  //   return $result;
  // }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if (!$answer['error']) {
      $xml = new SimpleXMLElement($answer['response']);
      $answer['status'] = (string) $xml->Result->Description;
      if (\Drupal::currentUser()->id() == 1) {
        // dsm($answer);
        // dsm($xml->Result->Description);
      }
    }
    else {
      $answer['status'] = 'Не известная ошибка';
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['answer']['errorCode'] == 0) {
      if ($answer['answer']['actionCode'] != 0) {
        $answer['status'] .= "<br> {$answer['answer']['actionCodeDescription']}";
      }
      $this->updatePayment($remote_id, $answer);
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function getOrderStatus($remote_id) {
    $result = [];
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($remote_id);
    $my_crc = strtoupper(md5("{$this->user}:{$payment->getRemoteId()}:$this->pass2"));
    $data = [
      'MerchantLogin' => $this->user,
      'InvoiceID' => $payment->getRemoteId(),
      'SignatureValue' => $my_crc,
    ];
    $url = self::ROBOCHECKURL . http_build_query($data);
    try {
      $answer = $this->client->get($url);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $response = $answer->getBody()->getContents();
        $result['error'] = FALSE;
        $result['response'] = $response;
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::logger('synpay_robokassa')->error($message);
      $result['error'] = TRUE;
      $result['response'] = $message;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_login"] = [
      '#type' => 'textfield',
      '#title' => "Идентификатор магазина",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_login"),
    ];
    $form["{$this->plugin_id}_live_pass"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий пароль #1",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_pass"),
    ];
    $form["{$this->plugin_id}_live_pass2"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий пароль #2",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_pass2"),
    ];
    $form["{$this->plugin_id}_test_pass"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый пароль #1",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_pass"),
    ];
    $form["{$this->plugin_id}_test_pass2"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый пароль #2",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_pass2"),
    ];
    $form["{$this->plugin_id}_description"] = [
      '#type' => 'textfield',
      '#title' => "Описание платежа (макс. 100 символов)",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_description"),
    ];
    $form["{$this->plugin_id}_send_receipt"] = [
      '#type' => 'select',
      '#title' => 'Передавать данные для формирования чека',
      '#options' => [
          'no' => 'Нет',
          'yes' => 'Да'
      ],
      '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    ];
    $form["{$this->plugin_id}_taxation"] = [
      '#type' => 'select',
      '#title' => 'Система налогообложения',
      '#options' => [
          'none' => 'Не выбрано',
          'osn' => 'ОСН',
          'usn_income' => 'Упрощенная СН (доходы)',
          'usn_income_outcome' => 'Упрощенная СН (доходы минус расходы)',
          'envd' => 'Единый налог на вмененный доход',
          'esn' => 'Единый сельскохозяйственный налог',
          'patent' => 'Патентная СН',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_taxation"),
    ];
    $form["{$this->plugin_id}_payment_method"] = [
      '#type' => 'select',
      '#title' => 'Признак способа расчёта',
      '#options' => [
          'none' => 'Не выбрано',
          'full_prepayment' => 'Предоплата 100%',
          'prepayment ' => 'Предоплата',
          'advance' => 'Аванс',
          'full_payment' => 'Полный расчёт',
          'partial_payment' => 'Частичный расчёт и кредит',
          'credit' => 'Передача в кредит',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_method"),
    ];
    $form["{$this->plugin_id}_payment_object"] = [
      '#type' => 'select',
      '#title' => 'Признак предмета расчёта',
      '#options' => [
          'none' => 'Не выбрано',
          'commodity' => 'Товар',
          'excise' => 'Подакцизный товар',
          'job' => 'Работа',
          'service' => 'Услуга',
          'payment' => 'Платёж',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_payment_object"),
    ];
    $form["{$this->plugin_id}_tax"] = [
      '#type' => 'select',
      '#title' => 'Налоговая ставка',
      '#options' => [
          'none' => 'Без НДС',
          'vat0' => 'НДС по ставке 0%',
          'vat10' => 'НДС по ставке 10%',
          'vat110' => 'НДС чека по расчетной ставке 10/110',
          'vat20' => 'НДС чека по ставке 20%',
          'vat120' => 'НДС чека по расчетной ставке 20/120',
          'vat12' => 'НДС по ставке 12% для клиентов из Казахстана',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_tax"),
    ];
    return $form;
  }

}
