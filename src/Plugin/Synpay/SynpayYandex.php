<?php

namespace Drupal\synpay\Plugin\Synpay;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\synpay\PluginManager\SynpayPluginBase;
use Drupal\synpay\PluginManager\SynpayPluginInterface;
use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use GuzzleHttp\Exception\ClientException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides Yandex Gateway.
 *
 * @SynpayAnnotation(
 *   id = "yandex",
 *   title = @Translation("Yandex"),
 * )
 */
class SynpayYandex extends SynpayPluginBase implements SynpayPluginInterface {
  const PAYMENT_EXPIRATION_PERIOD = '+1 hour';
  const LABEL = '"Яндекс пэй"';
  const HOST = "https://pay.yandex.ru/api/merchant/v1/";
  const HOST_TEST = "https://sandbox.pay.yandex.ru/api/merchant/v1/";
  // TRUE - цена с копейками, FALSE - цена в копейках.
  const PRECISION = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->plugin_id = $plugin_id;
    $this->title = $plugin_definition['title']->getUntranslatedString();
    $this->host = \Drupal::request()->getSchemeAndHttpHost();
    $this->currency = 'RUB';
    $this->lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->config = \Drupal::config('synpay.settings');
    $this->active = $this->config->get("{$plugin_id}_active");
    if (!$this->active) {
      \Drupal::messenger()->addWarning(t("Gateway is not active!"));
      return FALSE;
    }
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
    $gateways = $payment_gateway
      ->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    if (!empty($gateways)) {
      foreach ($payment_gateway->loadMultiple($gateways) as $id => $gateway) {
        $configuration = $gateway->getPluginConfiguration();
        if (array_key_exists('gateway', $configuration) && $configuration['gateway'] == $plugin_id) {
          $this->mode = $configuration['mode'];
          switch ($this->mode) {
            case 'test':
              $this->api = self::HOST_TEST;
              break;

            case 'live':
              $this->api = self::HOST;
              break;
          }
          $this->user = $this->config->get("{$plugin_id}_{$this->mode}_login");
          $this->pass = $this->config->get("{$plugin_id}_{$this->mode}_token");
        }
      }
    }
    $this->client = \Drupal::httpClient([
      'timeout'  => 10.0,
    ]);
    $this->headers = [
      'X-Request-Id' => Uuid::uuid4()->toString(),
      // 'X-Request-Timeout' => 10.0,
      'Authorization' => "Api-Key {$this->pass}",
      'Content-type' => 'application/json',
      'Cache-Control' => 'no-cache',
    ];
    $this->availablePaymentMethods = ['CARD'];
  }

  /**
   * {@inheritdoc}
   */
  public function debug() {
    $order_id = 564;
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
    $order = $storage->load($order_id);
    $parameters = [
      'availablePaymentMethods' => $this->availablePaymentMethods,
      'cart' => [
        'externalId' => $order_id,
        'items' => $this->getItems($order),
        'total' => [
          'amount' => $this->getAmount($order),
        ],
      ],
      'currencyCode' => $this->currency,
      'orderId' => $order_id,
      'redirectUrls' => [
        'onSuccess' => "{$this->host}/synpay/return_direct/{$this->plugin_id}",
        'onError' => "{$this->host}/synpay/return_direct/{$this->plugin_id}?fail",
      ],
    ];
    dsm($parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function pay(string $order, int $total) {
    // \Drupal::logger(__METHOD__ . __LINE__)->notice(
    // '@j', ['@j' => json_encode($order ?? [])]
    // );
    // $order = Uuid::uuid4()->getHex()->toString();
    // $order_amount = bcmul($total, 100, 0);
    // $params = [
    //   'headers' => $this->headers,
    //   'TerminalKey' => trim($this->user),
    //   'Amount' => $order_amount,
    //   'OrderId' => $order,
    //   'SuccessURL' => "{$this->host}/synpay/return_direct/$this->plugin_id",
    //   'FailURL' => "{$this->host}/synpay/return_direct/$this->plugin_id?fail",
    // ];
    // try {
    //   $response = $this->client->post("{$this->api}Init", ['json' => $params]);
    //   $code = $response->getStatusCode();
    //   $result = Json::decode($response->getBody()->getContents());
    // }
    // catch (ClientException $exception) {
    //   $message = $exception->getMessage();
    //   \Drupal::messenger()->addError($message);
    // }
  }

  /**
   * {@inheritdoc}
   */
  public function registerOrder($order_id, PaymentInterface $payment, $params) {
    if (is_null($this->client)) {
      return FALSE;
    }
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    $parameters = [
      'availablePaymentMethods' => $this->availablePaymentMethods,
      'cart' => [
        'externalId' => $order_id,
        'items' => $this->getItems($order),
        'total' => [
          'amount' => $this->getAmount($order),
        ],
      ],
      'currencyCode' => $this->currency,
      'orderId' => $order_id,
      'redirectUrls' => [
        'onSuccess' => "{$this->host}/synpay/return_direct/{$this->plugin_id}",
        'onError' => "{$this->host}/synpay/return_direct/{$this->plugin_id}?fail",
      ],
    ];
    $options = [
      'headers' => $this->headers,
      'body' => Json::encode($parameters),
    ];
    try {
      $answer = $this->client->post("{$this->api}orders", $options);
      $code = $answer->getStatusCode();
      if ($code == 200) {
        $body = $answer->getBody()->getContents();
        $response = JSON::decode($body);
        if ($response['status'] == 'success') {
          return [
            'formUrl' => $response['data']['paymentUrl'],
            'orderId' => $order_id,
          ];
        }
        else {
          \Drupal::messenger()->addError($response['Message']);
          \Drupal::messenger()->addError($response['Details']);
          return FALSE;
        }
      }
    }
    catch (ActionException $exception) {
      $message = $exception->getMessage();
      \Drupal::messenger()->addError($message);
      \Drupal::messenger()->addWarning(t('Check login/password to your payment gateway!'));
    }
    return FALSE;
  }

  /**
   * Получить позиции товара и корректировки (доставка).
   */
  public function getItems(OrderInterface $order) {
    $items = [];
    $c = 0;
    foreach ($order->getItems() as $item) {
      $price = $item->getAdjustedUnitPrice()->getNumber();
      $price = $this->numberFormat($price);
      $total = $item->getAdjustedTotalPrice()->getNumber();
      $total = $this->numberFormat($total);
      $items[$c] = [
        'productId' => $item->getPurchasedEntityId(),
        'title' => $item->getTitle(),
        'unitPrice' => $price,
        'quantity' => [
          'count' => $item->getQuantity(),
        ],
        'total' => $total,
        'receipt' => [
          'tax' => (int) $this->config->get("{$this->plugin_id}_tax"),
        ],
      ];
      $c++;
    }
    return $items;
  }

  /**
   * Get amount.
   */
  protected function getAmount(OrderInterface $order) : string {
    $price = $order->getTotalPrice()->getNumber();
    $adjustments_shipping = $order->getAdjustments(['shipping']);
    foreach ($adjustments_shipping as $adj) {
      $ship_cost = $adj->getAmount()->getNumber();
      $price -= $ship_cost;
    }
    return $this->numberFormat($price);
  }

  /**
   * Get amount.
   */
  protected function numberFormat(float $number) : string {
    return number_format($number, 2, '.', '');
  }

  /**
   * Commerce Return.
   */
  public function onReturn(string $remote_id) {
    // @todo Протестить, переделать.
    $payment_lock_name = $this->acquirePaymentLockName($order->id());
    $lock = \Drupal::lock();
    if (!$lock->acquire($payment_lock_name, 9.0)) {
      $lock->wait($payment_lock_name, 9.0);
      $lock->acquire($payment_lock_name, 9.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    $success = FALSE;
    foreach ($payments as $payment) {
      $status = $this->checkPayment($payment->remote_id->value);
      $answer = $this->updatePayment($payment, $status);
      if ($answer == 'completed' && !$success) {
        $success = TRUE;
      }
    }
    $result = $success ? 'completed' : $answer;
    return $result;
  }

  /**
   * Pay callback.
   */
  public function callback(Request $request) {
    $params = $request->query->all();
    if (!isset($params['fail'])) {
      return ['status' => 'completed'];
    }
    return ['status' => 'fail'];
  }

  /**
   * Commerce Return.
   */
  public function onNotify(Request $request) {
    // @todo Протестить, если не работает переделать на notify_direct.
    // Яндекс добавляет в ссылку /v1/webhook. Сделать редирект на нормальный путь.
    $payment_info = $this->getPaymentInfo($request);
    if (!$payment_info) {
      return new Response('Bad request', 400);
    }
    if ($payment_info->event != 'ORDER_STATUS_UPDATED') {
      return new Response('OK', 200);
    }
    if ($payment_info->merchantId != $this->user) {
      return new Response('Bad request', 400);
    }
    $lock = \Drupal::lock();
    $orderId = $payment_info->order->orderId;
    if (strpos($orderId, '/') !== FALSE) {
      $orderId = strstr($payment_info->order->orderId, '/', TRUE);
    }
    $payment_lock_name = $this->acquirePaymentLockName($orderId);
    if (!$lock->acquire($payment_lock_name, 7.0)) {
      $lock->wait($payment_lock_name, 7.0);
      $lock->acquire($payment_lock_name, 7.0);
    }
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(['remote_id' => $orderId]);
    $payment = reset($payments);
    if (!$payment) {
      return new Response('Bad request', 400);
    }
    $status = $payment_info->order->paymentStatus ?? '';
    switch ($status) {
      case 'CAPTURED':
      case 'AUTHORIZED':
        $payment->setState('completed');
        $payment->setCompletedTime(time());
        break;

      case 'REJECTED':
        $payment->setState('authorization_voided');
        break;
    }
    $payment->setRemoteState($status);
    $payment->save();

    return new Response('OK', 200);
  }

  /**
   * Get Payment Info.
   */
  public function getPaymentInfo(Request $request) :? object {
    $payment_info = NULL;
    $content = $request->getContent();
    if ($content) {
      $client = \Drupal::httpClient();
      $jwks_urls = [
        'prod' => 'https://pay.yandex.ru/api/jwks',
        'sandbox' => 'https://sandbox.pay.yandex.ru/api/jwks',
      ];
      foreach ($jwks_urls as $url) {
        if ($payment_info) {
          break;
        }
        $keys_json = $client->get($url)->getBody();
        $keys_data = json_decode($keys_json, JSON_OBJECT_AS_ARRAY);
        $keys = JWK::parseKeySet($keys_data);
        try {
          $payment_info = JWT::decode($content, $keys);
        }
        catch (\Throwable $th) {
        }
      }
    }
    return $payment_info;
  }

  /**
   * {@inheritdoc}
   */
  private function acquirePaymentLockName(string $orderId) : string {
    return sprintf('payment-lock-name-%d', $orderId);
  }

  /**
   * Check payments status.
   */
  public function checkOrderStatus($remote_id) {
    $answer = $this->checkPayment($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Update payments status.
   */
  public function updateOrderStatus($remote_id) {
    $answer = $this->getOrderStatus($remote_id);
    if ($answer['Success']) {
      $answer['status'] = $answer['Status'];
      $this->updatePayment($remote_id, $answer);
    }
    else {
      $answer['status'] = "Ошибка {$answer['ErrorCode']}. {$answer['Message']}";
      $answer['status'] .= "<br>{$answer['Details']}";
    }
    return $answer;
  }

  /**
   * Check Payments.
   */
  public function checkPayment($id) {
    $params = [
      'headers' => $this->headers,
    ];
    try {
      $response = $this->client->get("{$this->api}orders/$id", $params);
      $code = $response->getStatusCode();
      $answer = Json::decode($response->getBody()->getContents());
      dsm($answer);
      return $answer;
    }
    catch (ClientException $exception) {
      $message = $exception->getMessage();
      dsm($message);
      // \Drupal::messenger()->addError($message);
    }
    return FALSE;
  }

  /**
   * UpdatePayment.
   */
  private function updatePayment($payment, $status) {
    if (!$status) {
      $payment->state = 'authorization_voided';
      $payment->setRemoteState('Error');
      $result = 'authorization_voided';
    }
    else {
      switch ($status['data']['order']['paymentStatus'] ?? '') {
        case 'CAPTURED':
        case 'AUTHORIZED':
          $result = 'completed';
          break;

        default:
          $result = 'authorization_voided';
          break;
      }
      $payment->setState($result);
      $payment->setRemoteState($status['Status']);
    }
    $payment->save();
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function requestRefund(PaymentInterface $payment, $amount, $items) :? array {
    return [
      'error' => 404,
      'text' => 'Функционал не сделан.',
    ];
  }

  /**
   * Test cards.
   */
  public function testCards() {
    return "Карта для \"Успешного платежа\":<br>
            Номер: 4300 0000 0000 0777, Срок действия: 11/22, CVC2/CVV2 код: любой<br>
            Карта для \"Неуспешного платежа\":<br>
            Номер: 5000 0000 0000 0009, Срок действия: 11/22, CVC2/CVV2 код: любой";
  }

  /**
   * Setting Form.
   */
  public function settingForm() {
    $config = \Drupal::config('synpay.settings');
    $form["{$this->plugin_id}_widget"] = [
      '#type' => 'checkbox',
      '#title' => "Виджет  $this->title",
      '#default_value' => $config->get("{$this->plugin_id}_widget"),
    ];
    $form["{$this->plugin_id}_test_login"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_login"),
    ];
    $form["{$this->plugin_id}_test_token"] = [
      '#type' => 'textfield',
      '#title' => "Тестовый токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_test_token"),
    ];
    $form["{$this->plugin_id}_live_login"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий логин $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_login"),
    ];
    $form["{$this->plugin_id}_live_token"] = [
      '#type' => 'textfield',
      '#title' => "Рабочий токен $this->title",
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $config->get("{$this->plugin_id}_live_token"),
    ];
    // $form["{$this->plugin_id}_send_receipt"] = [
    //   '#type' => 'select',
    //   '#title' => 'Передавать данные для формирования чека',
    //   '#options' => [
    //       'no' => 'Нет',
    //       'yes' => 'Да'
    //   ],
    //   '#default_value' => $config->get("{$this->plugin_id}_send_receipt"),
    // ];
    // $form["{$this->plugin_id}_taxation"] = [
    //   '#type' => 'select',
    //   '#title' => 'Система налогообложения',
    //   '#options' => [
    //     'osn' => 'Общая',
    //     'usn_income' => 'Упрощенная (доходы)',
    //     'usn_income_outcome' => 'Упрощенная (доходы минус расходы)',
    //     'patent' => 'Патентная',
    //     'envd' => 'Единый налог на вмененный доход',
    //     'esn' => 'Единый сельскохозяйственный налог',
    //   ],
    //   '#default_value' => $config->get("{$this->plugin_id}_taxation"),
    // ];
    $form["{$this->plugin_id}_tax"] = [
      '#type' => 'select',
      '#title' => 'Ставка НДС',
      '#options' => [
        '1' => 'НДС по ставке 20%',
        '2' => 'НДС по ставке 10%',
        '3' => 'НДС по расчетной ставке 20/120',
        '4' => 'НДС по расчетной ставке 10/110',
        '5' => 'НДС по ставке 0%',
        '6' => 'без НДС',
      ],
      '#default_value' => $config->get("{$this->plugin_id}_tax"),
    ];
    // dsm($form);
    return $form;
  }

}
