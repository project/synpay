# _Synpay_

## Чем занимается?

> Добавляет настройку оплаты товара через платёжные системы.
> Доступные платёжные системы:
>
> - Альфа-банк
> - CloudPayment
> - Pay Keeper / Pay Keeper Qr
> - Робокасса
> - Сбербанк /Qr /Кредит/ Рассрочка
> - СеверГазБанк
> - Т-банк (Тинькофф)/Кредит /Долями /Qr
> - Yandex / Split
> - Ю-Касса / Qr

## Требования к платформе

- _Drupal 8-11_
- _PHP 7.4.0+_

## Нужны модули

- `composer require 'voronkovich/sberbank-acquiring-client'`
- `composer require 'yoomoney/yookassa-payout-sdk-php'`
- `composer require ramsey/uuid`

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/synpay/)

```sh
composer require 'drupal/synpay'
```

- [Drupal.org dev версия](https://www.drupal.org/project/synpay/releases/8.x-1.x-dev)

```sh
composer require 'drupal/synpay:1.x-dev@dev'
```

## Как использовать?

- В форме настроек модуля (Конфигурация > Система > Synpay). Или перейти по адресу _/admin/config/synpay/settings_. Выбрать активные платежные системы и заполнить необходимые параметры.
- В настройках платёжных шлюзов (Торговля > Конфигурация > Оплата > Платежные шлюзы). Или перейти по адресу _/admin/commerce/config/payment-gateways_. Добавить платежные шлюзы и указать в них выбранные платежные системы.

## Require

- `composer require 'voronkovich/sberbank-acquiring-client'`
- `composer require 'yoomoney/yookassa-payout-sdk-php'`
- `composer require ramsey/uuid`

### Synpay Plugins notify/return links.

- notify
  `https://YOURSITE/payment/notify/synpay`
- return
  `https://YOURSITE/synpay/return_direct/PLUGIN_ID`

### Robokassa notify/return links.

- notify
  `https://YOURSITE/synpay/callback/robokassa`
- return
  `https://YOURSITE/synpay/return/robokassa`
